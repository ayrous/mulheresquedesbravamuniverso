package br.com.senai.stdevelopment;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
//import android.support.annotation.RequiresApi;
import androidx.annotation.RequiresApi;
//import android.support.design.widget.FloatingActionButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import android.view.View;
//import android.support.design.widget.NavigationView;
import com.google.android.material.navigation.NavigationView;
//import android.support.v4.view.GravityCompat;
import androidx.core.view.GravityCompat;
//import android.support.v4.widget.DrawerLayout;
import androidx.drawerlayout.widget.DrawerLayout;
//import android.support.v7.app.ActionBarDrawerToggle;
import androidx.appcompat.app.ActionBarDrawerToggle;
//import android.support.v7.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatActivity;
//import android.support.v7.widget.Toolbar;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import br.com.senai.stdevelopment.modelo.Posts;

public class SobreNosActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private TextView apresentacao;
    private ImageView podcast;
    private ImageView midias;
    private ImageView estudar;
    private TextView btnPodAula;
    private TextView btnMedia;
    private TextView btnMedia2;
    private Button btnAulas;

    private  FirebaseAuth userAuth;
    private DatabaseReference databaseReference;

    private String usuarioId;
    private String nomeUsuarioLogado;

    //Popup
    private Dialog popup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sobrenos);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        userAuth = FirebaseAuth.getInstance();
        usuarioId = userAuth.getCurrentUser().getUid();
        databaseReference = FirebaseDatabase.getInstance().getReference().child("users").child("User");

            Bundle extras = getIntent().getExtras();

            apresentacao = findViewById(R.id.textBemVindo);
            btnPodAula = findViewById(R.id.textViewAbrirPodcast);
            btnMedia = findViewById(R.id.textViewAbrirMidia01);
            btnMedia2 = findViewById(R.id.textViewAbrirMidia02);
            btnAulas = findViewById(R.id.btnStudy);
            podcast = findViewById(R.id.imageView16);
            midias = findViewById(R.id.imageMidiaAcademica);
            estudar = findViewById(R.id.imageStudy);

            //tela de popup
            popup = new Dialog(this);


            databaseReference.child(usuarioId).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    if(dataSnapshot.exists()){
                    //    nomeUsuarioLogado = dataSnapshot.child("username").getValue().toString();

//                        Toast.makeText(getApplicationContext(), "Bem Vindx, " + nomeUsuarioLogado + "!", Toast.LENGTH_SHORT).show();
                        Toast.makeText(getApplicationContext(), "Escolha uma das opções!", Toast.LENGTH_LONG).show();

                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });




            btnMedia.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(SobreNosActivity.this, MediaActivity.class);
                    startActivity(intent);
                }
            });


        btnMedia2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SobreNosActivity.this, MediaActivity.class);
                startActivity(intent);
            }
        });



        midias.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SobreNosActivity.this, MediaActivity.class);
                startActivity(intent);
            }
        });


        btnPodAula.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(SobreNosActivity.this, PodcastActivity.class);
                    startActivity(intent);
                }
            });


        podcast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SobreNosActivity.this, PodcastActivity.class);
                startActivity(intent);
            }
        });



        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SobreNosActivity.this, MensagemActivity.class);
                startActivity(intent);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


    }


    //MINHA FUNÇÃO -> Mostra popup
    public  void showPopup(View v){

        TextView fechar;
        Button btnAulas;
        Button btnVideosFísica;

        popup.setContentView(R.layout.popup_customizado);
        fechar = (TextView) popup.findViewById(R.id.textViewPopup);
        btnAulas = (Button) popup.findViewById(R.id.btnBct);
        btnVideosFísica = (Button) popup.findViewById(R.id.btnFisicMais);

        fechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });

        btnAulas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SobreNosActivity.this, AulasActivity.class);
                startActivity(intent);

            }
        });

        btnVideosFísica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(SobreNosActivity.this, "Recurso indisponível!", Toast.LENGTH_SHORT).show();

            }
        });


        popup.show();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main2, menu);
        return true;
    }


    @Override
    protected void onStart() {
        super.onStart();

        FirebaseUser firebaseUser = userAuth.getCurrentUser();

        if(firebaseUser == null){
            Intent intent = new Intent(SobreNosActivity.this, TelaPrincipalActivity.class);
            finish();
            startActivity(intent);
        }else{
            checkUserExists();
        }

    }



    private void checkUserExists() {


        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if(!dataSnapshot.hasChild(usuarioId)){
                    Toast.makeText(SobreNosActivity.this, "ainda nn tem cadastro midias", Toast.LENGTH_SHORT).show();
                    //leva para criar usuario de midias
                    encaminhaParaCadastroMidias();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    //MÉTODO CRIADO POR MIM - ENCAMINHA PARA TELA DE CADASTRO DO USUARIO DAS MIDIAS
    private void encaminhaParaCadastroMidias() {

        Intent intent = new Intent(SobreNosActivity.this, CadastroMidiasActivity.class);
        finish();
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Toast.makeText(getApplicationContext(), "Opção desabilitada!", Toast.LENGTH_LONG).show();
            return true;
        }

        Toast.makeText(getApplicationContext(), "Em breve teremos essa opção!", Toast.LENGTH_LONG).show();

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if(id == R.id.nav_logout){
            Toast.makeText(getApplicationContext(), "Volte sempre!", Toast.LENGTH_LONG).show();
            FirebaseAuth.getInstance().signOut();
            startActivity(new Intent(SobreNosActivity.this, TelaPrincipalActivity.class));
            finish();

        }else if (id == R.id.nav_sobreNos) {
            Toast.makeText(getApplicationContext(), "Você já está nessa tela!", Toast.LENGTH_LONG).show();

        } else if (id == R.id.nav_historia) {
            Intent intent = new Intent(SobreNosActivity.this, HistoriaActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_contato) {
            Intent intent = new Intent(SobreNosActivity.this, ContatoActivity.class);
            startActivity(intent);


        } else if (id == R.id.nav_mensagem) {
            Intent intent = new Intent(SobreNosActivity.this, MensagemActivity.class);
            startActivity(intent);

        } else if(id == R.id.nav_yout){
            Toast.makeText(getApplication(), "Você será redirecionado!", Toast.LENGTH_LONG).show();
            Uri uri = Uri.parse("https://www.youtube.com/channel/UCVask_OFnc7MeYMWVV86M-Q");
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
