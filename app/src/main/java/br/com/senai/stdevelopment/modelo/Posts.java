package br.com.senai.stdevelopment.modelo;

import java.security.spec.PSSParameterSpec;
import java.util.List;
import java.util.Map;

/**
 * Created by Acer on 08/07/2020.
 */

public class Posts {


    //OBS: usar os msm noezinhos que estão na Firebase
    public String data, descricao, hora, imagemPost, uid, usuarioFoto, usuarioNome;
    public int contador;
    Map<String,Boolean> Likes;

    public Posts(){

    }

    //construtor
    public Posts(String data, String descricao, String hora, String imagemPost, String uid, String usuarioFoto, String usuarioNome, Map<String,Boolean> Likes, int contador) {
        this.data = data;
        this.descricao = descricao;
        this.hora = hora;
        this.imagemPost = imagemPost;
        this.uid = uid;
        this.usuarioFoto = usuarioFoto;
        this.usuarioNome = usuarioNome;
        this.Likes = Likes;
        this.contador = contador;
    }


    //Getters&Setters
    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getImagemPost() {
        return imagemPost;
    }

    public void setImagemPost(String imagemPost) {
        this.imagemPost = imagemPost;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUsuarioFoto() {
        return usuarioFoto;
    }

    public void setUsuarioFoto(String usuarioFoto) {
        this.usuarioFoto = usuarioFoto;
    }

    public String getUsuarioNome() {
        return usuarioNome;
    }

    public void setUsuarioNome(String usuarioNome) {
        this.usuarioNome = usuarioNome;
    }

    public Map<String,Boolean>  getLikes() {
        return Likes;
    }

    public void setLikes(Map<String,Boolean>  Likes) {
        this.Likes = Likes;
    }

    public int getContador() {
        return contador;
    }

    public void setContador(int contador) {
        this.contador = contador;
    }
}
