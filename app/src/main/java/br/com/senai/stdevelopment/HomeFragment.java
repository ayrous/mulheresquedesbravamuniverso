        package br.com.senai.stdevelopment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
//import android.support.annotation.Nullable;
import androidx.annotation.Nullable;
//import android.support.annotation.RequiresApi;
import androidx.annotation.RequiresApi;
//import android.support.v4.app.Fragment;
import androidx.fragment.app.Fragment;
//import android.support.v4.view.GravityCompat;
//import android.support.v4.widget.DrawerLayout;
//import android.support.v7.widget.LinearLayoutManager;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import br.com.senai.stdevelopment.adapter.StoryAdapter;
import br.com.senai.stdevelopment.modelo.Comentarios;
import br.com.senai.stdevelopment.modelo.Posts;
import br.com.senai.stdevelopment.modelo.Story;

/**
 * Created by Acer on 08/07/2020.
 */

public class HomeFragment extends Fragment implements View.OnClickListener{

//    private ImageButton imgAddPost;
    private RecyclerView postList, listaComentarios;
    private DatabaseReference postsReference, likesReference, usuarioReference;

    private RecyclerView recyclerViewStory;
    private StoryAdapter storyAdapter;
    private List<Story> listaStories;

    private RecyclerView recyclerViewComentarios;
    private List<Comentarios> listaComentario;

    private FirebaseAuth auth;
    private String usuarioId;
    String usuarioFotoBanco;

    private Boolean likeChecked = false;
    //Mapa que contém as tags de likes do usuário
    private Map<String,Boolean> g_mapTagsLikesUsuario;
    //Popup
    private Dialog popup;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_home,container,false);

        auth = FirebaseAuth.getInstance();
        usuarioId = auth.getCurrentUser().getUid();

        postList = (RecyclerView) view.findViewById(R.id.recyclerViewDois);
        postList.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(view.getContext());
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        postList.setLayoutManager(linearLayoutManager);

        postsReference = FirebaseDatabase.getInstance().getReference().child("users").child("Post");
        likesReference = FirebaseDatabase.getInstance().getReference().child("users").child("Likes");
        usuarioReference = FirebaseDatabase.getInstance().getReference().child("users").child("User");

        //Stories
        recyclerViewStory = view.findViewById(R.id.recyclerViewStory);
        recyclerViewStory.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL, false);
        recyclerViewStory.setLayoutManager(linearLayoutManager1);

        listaStories = new ArrayList<>();
        storyAdapter = new StoryAdapter(getContext(), listaStories);
        recyclerViewStory.setAdapter(storyAdapter);

        //tela de popup
    //    popup = new Dialog(getContext());


/**        imgAddPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "clicou", Toast.LENGTH_SHORT).show();
                SendUserToPostActivity();

            }
        });
**/
        atualizarTagsLikesUsuario();
        displayAllUsersPost();
        displayStories();

        return view;

    }


    //Método criado por mim - mostra todos os posts criados até o momento!
    private void displayAllUsersPost() {
        Query queryOrdenarPostsData = postsReference.orderByChild("contador");

        //Define a ordenação dos posts e configura o adaptador
        FirebaseRecyclerOptions<Posts> postsAdapterOptions =
                new FirebaseRecyclerOptions.Builder<Posts>()
                        .setQuery(queryOrdenarPostsData, Posts.class)
                        .setLifecycleOwner(this)
                        .build();

        FirebaseRecyclerAdapter postsRecyclerAdapter = new FirebaseRecyclerAdapter<Posts, PostsViewHolder>(postsAdapterOptions){
            @Override
            public PostsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                //Infla o layout dos posts para que ele possa ser populado no onBindViewHolder
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.lista_de_posts, parent, false);

                return new PostsViewHolder(view);
            }

            //Nota: esta função exerce a mesma função da populateViewHolder (depreciada com a atualização da FirebaseUI)
            @Override
            protected void onBindViewHolder(final PostsViewHolder viewHolder, int position, final Posts model) {
                Log.i("alerta", "posicção: " + position);
                final String postKey = getRef(position).getKey();

                DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("users").child("User").child(model.getUid());
                databaseReference.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        //Algoritmo para filtrar os posts baseando nas escolhas do usuário (presentes no Firebase)
                        Map<String, Boolean> mapPostTagsLikes = model.getLikes();
                        Map<String, Boolean> mapTagsLikesUsuario = g_mapTagsLikesUsuario;
                        Boolean likesMatch = false;

                        //Obtém as tags de likes do post e as itera comparando com as tags de likes do usuário
                        //Se uma tag for verdadeira em ambos os lados, o post será mostrado
                        for (String likesTag : mapPostTagsLikes.keySet()) {
                            if (mapPostTagsLikes.get(likesTag) && mapTagsLikesUsuario.get(likesTag)) {
                                likesMatch = true;
                            }
                        }

                        if (likesMatch) {
                            //62  77  112
                            final String nomeUsuarioBanco = dataSnapshot.child("username").getValue().toString();
                            final String nomeCompletoUsuarioBanco = dataSnapshot.child("nomecompleto").getValue().toString();
                            //     final String userId = dataSnapshot.child("uid").getValue().toString();
                            final String usuarioFotoBanco2 = dataSnapshot.child("profileImg").getValue().toString();
                            usuarioFotoBanco = usuarioFotoBanco2;

                            //Setando coisas os posts que podem mudar ao usuário modificar seu perfil
                            viewHolder.setUsuarioNome(nomeUsuarioBanco);

                            viewHolder.setUsuarioFoto(getContext(), usuarioFotoBanco);

                            //Setando coisas dos posts que não mudam ao alterar o perfil
                            viewHolder.setData(model.getData());
                            viewHolder.setDescricao(model.getDescricao());
                            viewHolder.setImagemPost(getContext(), model.getImagemPost());


                            //ao clicar no @ da user
                            viewHolder.txtNomeUsuario.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    //          Toast.makeText(getContext(), "@" + nomeUsuarioBanco, Toast.LENGTH_SHORT).show();
                                    Fragment fragmentUserProfile = new PerfilUsuarioFragment();
                                    Bundle bundle = new Bundle();
                                    bundle.putString("username", nomeUsuarioBanco);
                                    bundle.putString("userFoto", usuarioFotoBanco2);
                                    bundle.putString("userNomeCompleto", nomeCompletoUsuarioBanco);
                                    bundle.putString("userId", model.getUid());
                                    fragmentUserProfile.setArguments(bundle);

                                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                    fragmentTransaction.replace(R.id.fragment_container, fragmentUserProfile).commit();
                                }
                            });


                            //Comentários
                            viewHolder.imgCommentaPost.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Intent intentComentarios = new Intent(getContext(), ComentariosActivity.class);
                                    intentComentarios.putExtra("postKey", postKey);
                                    intentComentarios.putExtra("usuarioFoto", usuarioFotoBanco2);
                                    intentComentarios.putExtra("usuarioNome", nomeUsuarioBanco);
                                    intentComentarios.putExtra("descricao", model.getDescricao());
                                    startActivity(intentComentarios);
                                }
                            });

                            //Likes

                            //Recupera as curtidas do usuário e as exibe no adaptador
                            viewHolder.setLikeStatus(postKey, likeChecked);
                            //Quando clicarmos no botão de "like"
                            viewHolder.imgLikePost.setOnClickListener(new View.OnClickListener() {
                                @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                                @Override
                                public void onClick(View v) {
                                    likeChecked = true;
                                    likesReference.addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            if (likeChecked.equals(true)) {
                                                if (dataSnapshot.child(postKey).hasChild(usuarioId)) {

                                                    Log.d("aqi", postKey);

                                                    likesReference.child(postKey).child(usuarioId).removeValue();

                                                    viewHolder.imgLikePost.animate().rotationYBy(360f);
                                                    viewHolder.imgLikePost.animate().setDuration(1000);

                                                    //o que fzr quando a animação acabar:
                                                    Runnable endAction = new Runnable() {
                                                        public void run() {
                                                            viewHolder.imgLikePost.animate().setDuration(1000);
                                                            viewHolder.imgLikePost.animate().rotationYBy(3600f);
//                                viewHolder.imgLikePost.animate().translationXBy(360f);
                                                            viewHolder.imgLikePost.animate().start();
                                                            viewHolder.imgLikePost.animate().setStartDelay(200);
                                                            Log.d("passei","descurti");

                                                            viewHolder.imgLikePost.setImageResource(R.drawable.ic_btn_home_likepost);

                                                        }
                                                    };

                                                    viewHolder.imgLikePost.animate().withEndAction(endAction);
                                                    likeChecked = false;

                                                } else {

                                                    likesReference.child(postKey).child(usuarioId).setValue(true);

                                                    viewHolder.imgLikePost.animate().rotationYBy(360f);
                                                    viewHolder.imgLikePost.animate().setDuration(1000);

                                                    //o que fzr quando a animação acabar:
                                                    Runnable endAction = new Runnable() {
                                                        public void run() {
                                                            viewHolder.imgLikePost.animate().setDuration(1000);
                                                            viewHolder.imgLikePost.animate().rotationYBy(3600f);
                                                            //viewHolder.imgLikePost.animate().translationXBy(360f);
                                                            viewHolder.imgLikePost.animate().start();
                                                            viewHolder.imgLikePost.animate().setStartDelay(200);
                                                            Log.d("passei","curti");
                                                            viewHolder.imgLikePost.setImageResource(R.drawable.ic_btn_home_likepost_curtido);
                                                        }
                                                    };

                                                    viewHolder.imgLikePost.animate().withEndAction(endAction);
                                                    likeChecked = true;
                                                }

                                            }
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {

                                        }
                                    });
                                }
                            });

                        } else {
                            //Esconde o layout do post usando alguns parâmetros de layout
                            android.view.ViewGroup.LayoutParams params = viewHolder.v.getLayoutParams();
                            params.width = 0;
                            params.height = 0;
                            viewHolder.v.setLayoutParams(params);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        };

        postList.setAdapter(postsRecyclerAdapter);
    }


    public static class PostsViewHolder extends  RecyclerView.ViewHolder {

        View v;
        ImageView imgLikePost, imgCommentaPost;
        int contadorLikes;
        String idUsuarioLogado;
        DatabaseReference dbLikesReference;
        TextView txtNomeUsuario;

        //para acessar o que tem na view
        public PostsViewHolder(View itemView) {
            super(itemView);
            v = itemView;
            imgLikePost = (ImageView) v.findViewById(R.id.imageViewLikePost);
            imgCommentaPost = (ImageView) v.findViewById(R.id.imageViewBottaoComentar);
            dbLikesReference = FirebaseDatabase.getInstance().getReference().child("users").child("Likes");
            idUsuarioLogado = FirebaseAuth.getInstance().getCurrentUser().getUid();
            txtNomeUsuario = (TextView) v.findViewById(R.id.textPostUsuarioNome);
        }


        //setando o nome do usuario que fez o post
        public void setUsuarioNome(String usuarioNome) {

            TextView nomeUsuario = (TextView) itemView.findViewById(R.id.textPostUsuarioNome);
            nomeUsuario.setText(usuarioNome);
        }


        //setando a imagem de perfil do usuario que fez o post
        public void setUsuarioFoto(Context ctx, String usuarioFoto) {

            ImageView fotoUsuario = (ImageView) itemView.findViewById(R.id.profile_img_post);
            //Testando compressão local
            Picasso.with(ctx).load(usuarioFoto).fit().centerCrop().into(fotoUsuario);

        }

        //setando a Data
        public void setData(String data)
        {
            TextView dataPost = (TextView) itemView.findViewById(R.id.textTempoPostagem);
            dataPost.setText(data);
        }


        //SETANDO A DESCRIÇÃO
        public void setDescricao(String descricao) {

            TextView textoPost = (TextView) itemView.findViewById(R.id.textPostDescricao);
            textoPost.setText(descricao);
        }

        //Setando a imagem do post
        public void setImagemPost(Context ctx, String imagemPost)
        {
            ImageView postImagem = (ImageView) itemView.findViewById(R.id.imgPost);
            //Testando compressão local
            Picasso.with(ctx).load(imagemPost).resize(720,0).into(postImagem);

        }

        //Recupera as curtidas do usuário e as exibe no adaptador
        public void setLikeStatus(final String postKey, final Boolean likeCheck){

            dbLikesReference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    if(dataSnapshot.child(postKey).hasChild(idUsuarioLogado)){
                        //conta quantas pessoas já curtiram este post!!
                        contadorLikes = (int) dataSnapshot.child(postKey).getChildrenCount();
                        imgLikePost.animate().rotationYBy(360f);
                        imgLikePost.animate().setDuration(1000);
                        //o que fzr quando a animação acabar:
                        Runnable endAction = new Runnable() {
                            public void run() {
                                imgLikePost.animate().setDuration(1000);
                                imgLikePost.animate().rotationYBy(3600f);
                                //viewHolder.imgLikePost.animate().translationXBy(360f);
                                imgLikePost.animate().start();
                                imgLikePost.animate().setStartDelay(200);
                                imgLikePost.setImageResource(R.drawable.ic_btn_home_likepost_curtido);
                            }
                        };

                        imgLikePost.animate().withEndAction(endAction);
                        imgLikePost.setImageResource(R.drawable.ic_btn_home_likepost_curtido);
                        imgLikePost.setTag("liked");
                    }else{
                        contadorLikes = (int) dataSnapshot.child(postKey).getChildrenCount();
                        imgLikePost.animate().rotationYBy(360f);
                        imgLikePost.animate().setDuration(1000);

                        //o que fzr quando a animação acabar:
                        Runnable endAction = new Runnable() {
                            public void run() {
                                imgLikePost.animate().setDuration(1000);
                                imgLikePost.animate().rotationYBy(3600f);
//                                viewHolder.imgLikePost.animate().translationXBy(360f);
                                imgLikePost.animate().start();
                                imgLikePost.animate().setStartDelay(200);

                                imgLikePost.setImageResource(R.drawable.ic_btn_home_likepost);

                            }
                        };

                        imgLikePost.animate().withEndAction(endAction);
                        imgLikePost.setTag("like");
                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }



    @Override
    public void onClick(View v) {

    }

    private void displayStories(){

        DatabaseReference reference = FirebaseDatabase.getInstance().getReference().child("users").child("Story").child(FirebaseAuth.getInstance().getCurrentUser().getUid());
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                long timeAtual = System.currentTimeMillis();
                listaStories.clear();
                listaStories.add(new Story("", 0, 0,"",FirebaseAuth.getInstance().getCurrentUser().getUid()));

                storyAdapter.notifyDataSetChanged();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    //Conecta ao Firebase e atualiza as tags de likes do usuário, salvando numa variável de mapa global
    private void atualizarTagsLikesUsuario(){
        DatabaseReference dr = usuarioReference.child(usuarioId);
        dr.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                g_mapTagsLikesUsuario = (Map) dataSnapshot.child("Likes").getValue();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    //Dando os likes ou deslikes
    private void isLikes(String postId, final ImageView imageLike){

        final FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference().child("users").child("Likes").child(postId);

        dbRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if(dataSnapshot.child(firebaseUser.getUid()).exists()){
                    imageLike.animate().rotationYBy(360f);
                    imageLike.animate().setDuration(1000);

                    //o que fzr quando a animação acabar:
                    Runnable endAction = new Runnable() {
                        public void run() {
                            imageLike.animate().setDuration(1000);
                            imageLike.animate().rotationYBy(3600f);
//                                viewHolder.imgLikePost.animate().translationXBy(360f);
                            imageLike.animate().start();
                            imageLike.animate().setStartDelay(200);

                            imageLike.setImageResource(R.drawable.logo_ddu_sem_fundo);

                        }
                    };

                    imageLike.animate().withEndAction(endAction);

                    imageLike.setTag("liked");
                }else{
                    imageLike.setImageResource(R.drawable.logo_ddu_pb);
                    imageLike.setTag("like");
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


    //função para mostrar número de Likes
    private void numeroLikes(String postId) {
        DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference().child("users").child("Likes").child(postId);

        dbRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

//                numLikes.setText(dataSnapshot.getChildrenCount()+"likes");

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


}
