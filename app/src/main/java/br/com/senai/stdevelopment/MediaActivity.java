package br.com.senai.stdevelopment;

import android.content.Intent;
import android.os.Handler;
//import android.support.v7.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

public class MediaActivity extends AppCompatActivity {

    private static int SPLASH_SCREEN = 5000;

    //Variáveis
    Animation topAnimacao, bottomAnimacao;
    ImageView imagem;
    TextView tituloAnimacaoMidias, subtituloAnimacaoMidias;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //Animações
        topAnimacao = AnimationUtils.loadAnimation(this, R.anim.top_animation);
        bottomAnimacao = AnimationUtils.loadAnimation(this, R.anim.bottom_animacao);

        //Colocando as referencias
        imagem = findViewById(R.id.imagemEntradaLogoMulheres);
        tituloAnimacaoMidias = findViewById(R.id.textMidiasAnimation);
        subtituloAnimacaoMidias = findViewById(R.id.subTituloAnimacao);

        //Setando as animações
        imagem.setAnimation(topAnimacao);
        tituloAnimacaoMidias.setAnimation(bottomAnimacao);
        subtituloAnimacaoMidias.setAnimation(bottomAnimacao);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(MediaActivity.this,MediaEntradaActivity.class);
                startActivity(intent);
                finish();
            }
        }, SPLASH_SCREEN);
    }

}
