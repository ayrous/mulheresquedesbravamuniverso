package br.com.senai.stdevelopment;

import android.app.Dialog;
import android.os.Bundle;
//import android.support.annotation.NonNull;
import androidx.annotation.NonNull;
//import android.support.design.widget.BottomNavigationView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
//import android.support.v4.app.Fragment;
import androidx.fragment.app.Fragment;
//import android.support.v7.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.TextView;

public class MediaEntradaActivity extends AppCompatActivity {

    private TextView mTextMessage;

    private Dialog popup;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            Fragment fragmentSelecionado = null;

            switch (item.getItemId()) {
                case R.id.navigation_home:
                    fragmentSelecionado = new HomeFragment();

                    //setand para aparecer um site na entrada
//                    WebView myWebView = (WebView) findViewById(R.id.webView);
  //                  myWebView.loadUrl("https://www.journaldev.com/9333/android-webview-example-tutorial");

                    break;
                case R.id.navigation_dashboard:
                    fragmentSelecionado = new DashboardFragment();
                    break;
                case R.id.navigation_localiza:
                    fragmentSelecionado = new LocalizaFragment();
                    break;

                case R.id.navigation_plus:
                    fragmentSelecionado = new NovoPostFragment();
                    break;
            //        mTextMessage.setText(R.string.title_notifications);
//                    return true;
                case R.id.navigation_profile:
                    fragmentSelecionado = new ProfileFragment();
                    break;

            }

            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragmentSelecionado).commit();
            return true;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media_entrada);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //tela de popup
        popup = new Dialog(getApplicationContext());


        //  mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeFragment()).commit();
    }


    //MINHA FUNÇÃO -> Mostra popup
    public  void showPopupNews(View v){

        TextView fechar;
        WebView webView;


        popup.setContentView(R.layout.popup_news);
        fechar = (TextView) popup.findViewById(R.id.textViewPopupNews);
        webView = (WebView) popup.findViewById(R.id.webViewNews);
        webView.loadUrl("https://www.bbc.com/portuguese/topics/c340q4kdy9kt");

        fechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });

        popup.show();

    }

}
