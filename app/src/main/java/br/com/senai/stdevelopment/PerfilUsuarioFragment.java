package br.com.senai.stdevelopment;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import br.com.senai.stdevelopment.adapter.PerfilPostsAdapter;
import br.com.senai.stdevelopment.adapter.StoryAdapter;
import br.com.senai.stdevelopment.modelo.Posts;
import br.com.senai.stdevelopment.modelo.Story;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class PerfilUsuarioFragment extends Fragment implements View.OnClickListener{

    private ImageView imgProfileUsuario;
    private TextView txtusername, txtNomeCompleto;
    private DatabaseReference usuarioRefBanco, postRef;
    private String usuarioId, imgPost;

    private GridView listaPosts;
    private List<Posts> listaPostsUsuario;
    private PerfilPostsAdapter perfilPostsAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);




    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_perfil_usuario, container, false);


        imgProfileUsuario = (ImageView) view.findViewById(R.id.imagemPerfil);
        txtusername = (TextView) view.findViewById(R.id.textViewUserNamePerfil);
        txtNomeCompleto = (TextView) view.findViewById(R.id.nomeCompletoPerfil);

        listaPosts = (GridView) view.findViewById(R.id.gridPostsPerfil);
        listaPostsUsuario = new ArrayList<>();
        perfilPostsAdapter = new PerfilPostsAdapter(getContext(), listaPostsUsuario);

        String userNome = getArguments().get("username").toString();
        String userFoto = getArguments().get("userFoto").toString();
        String nomeComp = getArguments().get("userNomeCompleto").toString();
        usuarioId = getArguments().get("userId").toString();

        txtusername.setText(userNome);
        Picasso.with(getContext()).load(userFoto).into(imgProfileUsuario);
        txtusername.setText("  @"+userNome);
        txtNomeCompleto.setText(nomeComp);

        postRef = FirebaseDatabase.getInstance().getReference().child("users").child("Post");
        usuarioRefBanco = FirebaseDatabase.getInstance().getReference().child("users").child("User").child(usuarioId);

        Toast.makeText(getContext(), "@;"+userNome, Toast.LENGTH_SHORT).show();

        //GridView
        listaPosts.setAdapter(perfilPostsAdapter);
       displayPosts();


       displayLikesTags();

        listaPosts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //se clicar em um post X
                Toast.makeText(view.getContext(), "Clicou: "+ i, Toast.LENGTH_SHORT).show();
            }
        });


        return view;
    }

    @Override
    public void onClick(View view) {

    }


    //Método criado por mim - mostra todas as tags que a pessoa curte!
    private void displayLikesTags() {

        usuarioRefBanco.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot.exists()) {

                    int conta = 0;
                    for(DataSnapshot atributosTag : dataSnapshot.child("Likes").getChildren()){

                        String att = atributosTag.getValue().toString();

                        //bio
                        if(conta == 0){

                            if(att == "true"){
                                
                            }

                        }else if(conta == 1){

                        }else if (conta == 2){

                        }
                        Toast.makeText(getContext(), "att:"+att, Toast.LENGTH_SHORT).show();

                        conta++;
                    }
                    perfilPostsAdapter.notifyDataSetChanged();

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }




    //Método criado por mim - mostra todos os posts criados até o momento!
    private void displayPosts() {

        final Query ordenarPostsUsuario = postRef.orderByChild("uid").equalTo(usuarioId);

        ordenarPostsUsuario.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot.exists()) {

                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        // do with your result
                        Posts p = snapshot.getValue(Posts.class);

                        listaPostsUsuario.add(p);

                    }
                    perfilPostsAdapter.notifyDataSetChanged();

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }


}