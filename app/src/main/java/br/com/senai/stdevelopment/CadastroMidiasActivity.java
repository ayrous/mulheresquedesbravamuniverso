package br.com.senai.stdevelopment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
//import android.support.annotation.NonNull;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
//import android.support.v7.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

public class CadastroMidiasActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    private EditText userName;
    private EditText nomeCompleto;
    private EditText areaAtuacao;
    private Spinner spinnerAreas;
    private Button btnSalvar;
    private ImageButton imgPerfil;
    private String areaDeAtuacao;

    private FirebaseAuth firebaseAuth;
    private DatabaseReference databaseReference;
    private String usuarioId;

    private ProgressDialog loading;

    private Uri imgUri;

    private final static int GALERIA_PICKER = 1;

    private StorageReference imgReference;

    String downloadUrl, username, nomecomp,area;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_midias);

        userName = (EditText)findViewById(R.id.editCadastroUsername);
        nomeCompleto = (EditText) findViewById(R.id.editCadastroNomeCompleto);
        areaAtuacao = (EditText) findViewById(R.id.editCadastroAreaAtuacao);
        btnSalvar = (Button) findViewById(R.id.btnCadastroMidias);
        imgPerfil = (ImageButton) findViewById(R.id.imgProfile);


        spinnerAreas = findViewById(R.id.spinnerAreas);
        ArrayAdapter<CharSequence> adapterAreas = ArrayAdapter.createFromResource(this, R.array.areas, R.layout.support_simple_spinner_dropdown_item);
        adapterAreas.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinnerAreas.setAdapter(adapterAreas);
        spinnerAreas.setOnItemSelectedListener(this);


        firebaseAuth = FirebaseAuth.getInstance();
        usuarioId = firebaseAuth.getCurrentUser().getUid();
        databaseReference = FirebaseDatabase.getInstance().getReference().child("users").child("User").child(usuarioId);

        imgReference = FirebaseStorage.getInstance().getReference();


        loading = new ProgressDialog(this);

        btnSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                salvarInformacoesContaMidias();
            }
        });


        imgPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intentGaleria = new Intent();
                intentGaleria.setAction(Intent.ACTION_GET_CONTENT);
                intentGaleria.setType("image/*");
                startActivityForResult(intentGaleria,GALERIA_PICKER);

            }
        });

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists())
                {
                    if (dataSnapshot.hasChild("profileImage"))
                    {
                        String image = dataSnapshot.child("profileImage").getValue().toString();
                        Picasso.with(CadastroMidiasActivity.this).load(image).placeholder(R.drawable.portaria).into(imgPerfil);
                    }
                    else
                    {
                        Toast.makeText(CadastroMidiasActivity.this, "Selecione uma imagem antes...", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == GALERIA_PICKER && resultCode == RESULT_OK && data!=null){
            imgUri = data.getData();
            imgPerfil.setImageURI(imgUri);
        }
    }



    //MÉTODO CRIADO POR MIM - SALVA AS INF DO USUARIO LOGADO NO FIREBASE PARA MIDIAS
    private void salvarInformacoesContaMidias() {

        username = userName.getText().toString();
        nomecomp = nomeCompleto.getText().toString();
        area = areaDeAtuacao;

        //Validações:

        if(TextUtils.isEmpty(username)){

            Toast.makeText(this, "Insira seu username!", Toast.LENGTH_SHORT).show();

        } else if(TextUtils.isEmpty(nomecomp)){

            Toast.makeText(this, "Insira seu Nome!", Toast.LENGTH_SHORT).show();

        } else if(TextUtils.isEmpty(area)){
            Toast.makeText(this, "Fala pra gente sua área de atuação(ou qual pretende ir)!", Toast.LENGTH_SHORT).show();

        } else if(imgUri == null){

            Toast.makeText(this, "Coloque uma imagem!", Toast.LENGTH_SHORT).show();

        }else{

            //Salvando:

            loading.setTitle("Criando usuário");
            loading.setMessage("Aguarda ai rapa");
            loading.show();
            loading.setCanceledOnTouchOutside(true);

            //salvar imagem
            StorageReference filePath = imgReference.child("Profile Images").child(usuarioId + ".jpg");


            //Salvar a imagem na Firebase
            filePath.putFile(imgUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    filePath.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            downloadUrl = uri.toString();
                            Toast.makeText(CadastroMidiasActivity.this, "Imagem salva com sucesso!", Toast.LENGTH_SHORT).show();

                            databaseReference.child("profileImg").setValue(downloadUrl).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if(task.isSuccessful()){
                                        Toast.makeText(CadastroMidiasActivity.this, "Imagem salva no Firebase!", Toast.LENGTH_SHORT).show();
                                    }else{
                                        String msg = task.getException().getMessage();
                                        Toast.makeText(CadastroMidiasActivity.this, "vish: "+msg, Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });

                            //Salvando efetivamente o user:
                            HashMap usuarioMap = new HashMap();
                            usuarioMap.put("username", username);
                            usuarioMap.put("nomecompleto", nomecomp);
                            usuarioMap.put("areaatuacao", area);

                            databaseReference.updateChildren(usuarioMap).addOnCompleteListener(new OnCompleteListener() {
                                @Override
                                public void onComplete(@NonNull Task task) {

                                    if(task.isSuccessful()){
                                        Log.i("obs","passei");
                                        Intent intent = new Intent(CadastroMidiasActivity.this, TesteActivity.class);
                                        startActivity(intent);
                                        finish();

                                        Toast.makeText(CadastroMidiasActivity.this, "Cadastro finalizado!", Toast.LENGTH_LONG).show();
                                        loading.dismiss();
                                    }else{

                                        String msg = task.getException().getMessage();
                                        Toast.makeText(CadastroMidiasActivity.this, "Ops... "+msg, Toast.LENGTH_SHORT).show();
                                        loading.dismiss();
                                    }
                                }
                            });
                        }
                    });
                }

            });
        }
    }



    //Métodos implementados para o Spinner áreas
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        String areaSelecionada = parent.getItemAtPosition(position).toString();
        Toast.makeText(this, areaSelecionada, Toast.LENGTH_SHORT).show();

        if(areaSelecionada == "Outro"){
            areaAtuacao.setVisibility(View.VISIBLE);
            areaDeAtuacao = areaAtuacao.getText().toString();
        }else{
            areaDeAtuacao = areaSelecionada;
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}