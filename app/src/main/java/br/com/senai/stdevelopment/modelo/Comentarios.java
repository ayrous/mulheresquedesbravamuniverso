package br.com.senai.stdevelopment.modelo;

public class Comentarios {

    public String comentario;
    public String data;
    public String hora;
    public String username;

    public Comentarios(String comentario, String data, String hora, String username) {
        this.comentario = comentario;
        this.data = data;
        this.hora = hora;
        this.username = username;
    }

    public Comentarios(){

    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
