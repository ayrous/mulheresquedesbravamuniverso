package br.com.senai.stdevelopment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

//import android.support.annotation.NonNull;
//import android.support.v7.app.AppCompatActivity;

public class CriarUsuarioActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {


    private EditText editEmail;
    private  EditText editSenha;
    private  EditText editConfirmaSenha;
    private Button btnCadastrar;
    private Spinner spinnerAreas;

    private FirebaseAuth firebaseAuth;
    private ProgressDialog loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_criar_usuario);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarCad);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Cadastro");

        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getSupportActionBar().setHomeButtonEnabled(true);


        editEmail = (EditText) findViewById(R.id.edit_email_id2);
        editSenha = (EditText) findViewById(R.id.edit_senha_id2);
        editConfirmaSenha = (EditText) findViewById(R.id.edit_confirma_senha_id2);
        btnCadastrar = (Button) findViewById(R.id.btn_cadastrar_cadastro);

        //Spinner
        spinnerAreas = findViewById(R.id.spinnerAreas);
        ArrayAdapter<CharSequence> adapterAreas = ArrayAdapter.createFromResource(this, R.array.areas, R.layout.support_simple_spinner_dropdown_item);
        adapterAreas.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinnerAreas.setAdapter(adapterAreas);
        spinnerAreas.setOnItemSelectedListener(this);


        firebaseAuth = FirebaseAuth.getInstance();
        loading =  new ProgressDialog(this);


        btnCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                criarNovaConta();
            }
        });


    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) { //Botão adicional na ToolBar
        switch (item.getItemId()) {
            case android.R.id.home:  //ID do seu botão (gerado automaticamente pelo android, usando como está, deve funcionar
                startActivity(new Intent(this, TelaPrincipalActivity.class));  //O efeito ao ser pressionado do botão (no caso abre a activity)
                finishAffinity();  //Método para matar a activity e não deixa-lá indexada na pilhagem
                break;
            default:break;
        }
        return true;
    }

    private void criarNovaConta() {

        String email = editEmail.getText().toString();
        String senha = editSenha.getText().toString();
        String confirmaSenha = editConfirmaSenha.getText().toString();

        if (TextUtils.isEmpty(email)) {
            Toast.makeText(this, "Escreva seu email!", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(senha)) {
            Toast.makeText(this, "Escreva uma senha!", Toast.LENGTH_SHORT).show();
        }else if (TextUtils.isEmpty(confirmaSenha)) {
            Toast.makeText(this, "Confirme sua senha!", Toast.LENGTH_SHORT).show();
        } else if(!senha.equals(confirmaSenha)){
            Toast.makeText(this, "Senhas não batem!", Toast.LENGTH_SHORT).show();
        }else{

            loading.setTitle("Cadastrando...");
            loading.setMessage("Aguarde uns instantes..");
            loading.show();
            loading.setCanceledOnTouchOutside(true);

            firebaseAuth.createUserWithEmailAndPassword(email,senha).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if(task.isSuccessful()){

                        encaminharParaPagInicial();

                        Toast.makeText(CriarUsuarioActivity.this, "Usuário criado com sucesso!", Toast.LENGTH_SHORT).show();
                        loading.dismiss();
                    }else{
                        String msg = task.getException().getMessage();

                        Toast.makeText(CriarUsuarioActivity.this, "Algo deu errado: "+msg, Toast.LENGTH_SHORT).show();
                        loading.dismiss();
                    }
                }
            });
        }



    }


    @Override
    protected void onStart() {
        super.onStart();

        FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();

        if(firebaseUser!= null){
            Intent intent = new Intent(CriarUsuarioActivity.this, SobreNosActivity.class);
            finish();
            startActivity(intent);
        }
    }

    //MÉTODO CRIAOD POR MIM - encaminha o usuario para pag prinipal, após logado
    private void encaminharParaPagInicial() {

        Intent intent = new Intent(CriarUsuarioActivity.this, SobreNosActivity.class);
        finish();
        startActivity(intent);

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
