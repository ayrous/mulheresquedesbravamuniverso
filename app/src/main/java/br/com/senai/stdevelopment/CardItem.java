package br.com.senai.stdevelopment;

/**
 * Created by Acer on 01/07/2020.
 */

public class CardItem {

    int background;
    String nome_aulas;
    int aula_imagem;
    int acessos_aula;

    public CardItem() {
    }

    public CardItem(int background, String nome_aulas, int aula_imagem, int acesso_aula) {
        this.background = background;
        this.nome_aulas = nome_aulas;
        this.aula_imagem = aula_imagem;
        this.acessos_aula = acesso_aula;
    }

    public int getBackground() {
        return background;
    }

    public void setBackground(int background) {
        this.background = background;
    }

    public String getNome_aulas() {
        return nome_aulas;
    }

    public void setNome_aulas(String nome_aulas) {
        this.nome_aulas = nome_aulas;
    }

    public int getAula_imagem() {
        return aula_imagem;
    }

    public void setAula_imagem(int aula_imagem) {
        this.aula_imagem = aula_imagem;
    }

    public int getAcessos_aula() {
        return acessos_aula;
    }

    public void setAcessos_aula(int acessos_aula) {
        this.acessos_aula = acessos_aula;
    }
}
