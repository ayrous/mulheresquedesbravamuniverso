package br.com.senai.stdevelopment;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

public class AlertDialogo {

    private EditText editEmail;
    private TextView lblVoltar_Recuperacao;
    private Button btnRecuperar;

    public void criarAlertEsqueciSenha(final Context contexto){
        android.app.AlertDialog.Builder mBuider = new android.app.AlertDialog.Builder(contexto);
        LayoutInflater inflater = (LayoutInflater) contexto.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        final View mView = inflater.inflate(R.layout.alert_dialog_recuperacao,null);
        editEmail = (EditText) mView.findViewById(R.id.editRecuperaEmail);
        btnRecuperar = (Button) mView.findViewById(R.id.btnRecuperar);
        lblVoltar_Recuperacao = (TextView) mView.findViewById(R.id.lblVoltar_Recuperacao);


        // executando a aplicação
        mBuider.setView(mView);
        final android.app.AlertDialog dialogo = mBuider.create();
        dialogo.setCancelable(false);
        //definindo o background da aplicação como transparent, assim o radius do dialog aparece
        dialogo.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogo.show();

        lblVoltar_Recuperacao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogo.dismiss();
            }
        });

        btnRecuperar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!editEmail.getText().toString().isEmpty()){

                   /* ProgressDialog progress = new ProgressDialog(contexto);
                    progress.setTitle("Esqueci a senha");
                    progress.setMessage("Por favor, Aguarde enquanto enviamos o e-mail de recuperação");
                    progress.setCancelable(false);
                    progress.show();

                    dialogo.dismiss();*/


                }else{
                    Snackbar.make(view,"Preencha o campo para recuperar a senha", Snackbar.LENGTH_SHORT).show();
                }

            }
        });
    }


}
