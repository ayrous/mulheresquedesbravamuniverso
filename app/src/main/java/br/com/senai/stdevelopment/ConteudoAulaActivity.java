package br.com.senai.stdevelopment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.net.Uri;
//import android.support.design.widget.CollapsingToolbarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
//import android.support.design.widget.FloatingActionButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
//import android.support.graphics.drawable.VectorDrawableCompat;
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat;
//import android.support.v7.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import br.com.senai.stdevelopment.modelo.Mensagem;
import in.goodiebag.carouselpicker.CarouselPicker;

public class ConteudoAulaActivity extends AppCompatActivity {

    private static  final String TAG = "ConteudoAulaActivity";
    private ImageView imagemFundo;
    private CarouselPicker carouselPicker, carouselPicker2, carouselPicker3;
    private TextView ementa;
    private TextView tituloDisciplina;

    private CollapsingToolbarLayout collapsingToolbarLayout;
    private FloatingActionButton floatingActionButton;

    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conteudo_aula);
        Log.d(TAG, "clicouuu" );

        floatingActionButton = (FloatingActionButton) findViewById(R.id.floatingButton);

        collapsingToolbarLayout = findViewById(R.id.toolbarConteudo);
        carouselPicker = (CarouselPicker) findViewById(R.id.carouselTeste);


        //pegando os extras vindo do RecyclerView
        Intent intent = getIntent();
        String titulo = (String) intent.getSerializableExtra("titulo");
        int img = (int) intent.getSerializableExtra("img");
        tituloDisciplina = (TextView) findViewById(R.id.textTituloDisciplina);
        tituloDisciplina.setText(titulo);

        //Setando a imagem de fundo passada
        imagemFundo = (ImageView) findViewById(R.id.imgConteudo);

        Drawable d = getResources().getDrawable(img);
        Bitmap b = ((BitmapDrawable) d).getBitmap();

        imagemFundo.setImageBitmap(b);

        collapsingToolbarLayout.setTitle(titulo);


        ementa = (TextView) findViewById(R.id.textViewConteudoAula);

        List<CarouselPicker.PickerItem> imagensCarousel = new ArrayList<>();

        if(titulo.equals("ESTRUTURA DA MATÉRIA")){
            //mudando a cor da barrinha de cima
            collapsingToolbarLayout.setContentScrimColor(R.color.roxo);

            //Muda a cor do texto quando está expandido
            collapsingToolbarLayout.setExpandedTitleColor(R.color.colorDark);

            //Muda a cor do texto quando fica na parte de cima, qnd damos scroll
            collapsingToolbarLayout.setCollapsedTitleTextColor(R.color.colorDark);
            ementa.setText("Estrutura da matéria, ou “EM” funciona como uma ponte de alguns tópicos de " +
                    "química e física do ensino médio para o estudo de estruturas atômicas do ensino superior. " +
                    "Em “EM” o objetivo é relacionar propriedades macroscópicas da matéria com sua " +
                    "estrutura atômico e molecular.\n" +
                    "A disciplina trata da contextualização atômica da Estrutura da Matéria. Por ser uma das disciplinas " +
                    "introdutórias ao Bacharelado Interdisciplinar, o formalismo matemático dos tópicos abordados não é aprofundado," +
                    " dando-se ênfase à interpretação qualitativa das leis que regem o comportamento da matéria.");

            //Carousel1 e add suas imagens(no caso 3, pois definimos no xml que seriam 3, é o minimo)
            imagensCarousel.add(new CarouselPicker.DrawableItem(R.mipmap.estrutura_materia_foreground));
            imagensCarousel.add(new CarouselPicker.DrawableItem(R.mipmap.estrutura_materia_dois_foreground));
            imagensCarousel.add(new CarouselPicker.DrawableItem(R.mipmap.aulas_indisponiveis_foreground));

            floatingActionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    //redirecina para a Playlist
                    Toast.makeText(getApplicationContext(),"Bora pra playlist?!", Toast.LENGTH_LONG).show();

                    Uri uri = Uri.parse("https://youtube.com/playlist?list=PL9u-75TCsJ9UtVL7vpWQwdmdZ5Vx-8e7J");
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                    floatingActionButton.animate();
                }
            });

        } else if(titulo.equals("BASES MATEMÁTICAS")){

            collapsingToolbarLayout.setTitle("BM");

            //mudando a cor da barrinha de cima
            collapsingToolbarLayout.setContentScrimColor(R.color.roxo);

            //Muda a cor do texto quando está expandido
            collapsingToolbarLayout.setExpandedTitleColor(R.color.colorText);

            //Muda a cor do texto quando fica na parte de cima, qnd damos scroll
            collapsingToolbarLayout.setCollapsedTitleTextColor(R.color.colorDark);

            ementa.setText("Bases matemáticas, ou “BM”, serve como uma ponte para os conhecimentos de " +
                    "matemática do ensino médio para a matemática do ensino superior. A disciplina de Bases " +
                    "Matemática revisa conteúdos elementares da matemática, com ênfase nos conceitos relativos à " +
                    "função real, porém sobre um ponto de vista típico do ensino superior, desenvolvendo a capacidade de " +
                    "compreensão e uso linguagem matemática, do raciocínio lógico.\n" +
                    "Desse modo diminuindo as disparidades de formação dos ingressantes no BC&T e concomitantemente " +
                    "ressaltando a estrutura conceitual do conhecimento matemático.\n");

            //Carousel1 e add suas imagens(no caso 3, pois definimos no xml que seriam 3, é o minimo)
            imagensCarousel.add(new CarouselPicker.DrawableItem(R.mipmap.bases_matematicas_um_foreground));
            imagensCarousel.add(new CarouselPicker.DrawableItem(R.mipmap.bases_matematicas_dois_freground));
            imagensCarousel.add(new CarouselPicker.DrawableItem(R.mipmap.aulas_indisponiveis_foreground));

            floatingActionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    //redirecina para a Playlist
                    Toast.makeText(getApplicationContext(),"Bora pra playlist?!", Toast.LENGTH_LONG).show();

                    Uri uri = Uri.parse("https://youtube.com/playlist?list=PL9u-75TCsJ9WqYiuzUrMFMhy436iBNH78");
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                    floatingActionButton.animate();
                }
            });

            //Carrousel e add seus textos embaixo da imagem
//            textosCarouselUm.add(new CarouselPicker.TextItem("#0", 20)); //20: textsize (dp)
//            textosCarouselUm.add(new CarouselPicker.TextItem("#1", 20));
//            textosCarouselUm.add(new CarouselPicker.TextItem("#2", 20));


        } else if (titulo.equals("BASES COMPUTACIONAIS DA CIÊNCIA")) {

            collapsingToolbarLayout.setTitle("BCC");

            //Muda a cor do texto quando fica na parte de cima, qnd damos scroll
            collapsingToolbarLayout.setCollapsedTitleTextColor(R.color.colorWhite);

            //mudando a cor da barrinha de cima
            collapsingToolbarLayout.setContentScrimColor(R.color.roxo);

            ementa.setText("Que tal aprender um pouco de programação? Em “BCC” " +
                    "é possível compreender os conceitos básicos e fundamentais da computação, empregar a " +
                    "computação para a produção de conhecimento científico e interdisciplinar, familiarizar com o uso " +
                    "de diferentes tipos de ferramentas (softwares) computacionais, entender algoritmos e lógica de programação " +
                    "e entender sobre as etapas de simulação de sistemas.\n");

            //Carousel1 e add suas imagens(no caso 3, pois definimos no xml que seriam 3, é o minimo)
            imagensCarousel.add(new CarouselPicker.DrawableItem(R.mipmap.aulas_indisponiveis_foreground));
            imagensCarousel.add(new CarouselPicker.DrawableItem(R.mipmap.aulas_indisponiveis_foreground));
            imagensCarousel.add(new CarouselPicker.DrawableItem(R.mipmap.aulas_indisponiveis_foreground));

            floatingActionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    //redirecina para a Playlist
                    Toast.makeText(getApplicationContext(),"Aulas indisponíveis!", Toast.LENGTH_LONG).show();

                    floatingActionButton.animate();
                }
            });

        } else if(titulo.equals("BASES EXPERIMENTAIS DAS CIÊNCIAS NATURAIS")){

            collapsingToolbarLayout.setTitle("BECN");

            //mudando a cor da barrinha de cima
            collapsingToolbarLayout.setContentScrimColor(R.color.green);

            //Muda a cor do texto quando está expandido
            collapsingToolbarLayout.setExpandedTitleColor(R.color.colorText);

            //Muda a cor do texto quando fica na parte de cima, qnd damos scroll
            collapsingToolbarLayout.setCollapsedTitleTextColor(R.color.colorDark);


            ementa.setText("É muito legal já ter contato com laboratório desde o primeiro período da universidade!! E em " +
                    "Base Experimental, ou “BECN” temos esta oportunidade! É nesta matéria que ocorre a familiarização do aluno com o método científico e desenvolvimento de " +
                    "práticas experimentais interdisciplinares, por meio de prática em laboratório!\n");

            //Carousel1 e add suas imagens(no caso 3, pois definimos no xml que seriam 3, é o minimo)
            imagensCarousel.add(new CarouselPicker.DrawableItem(R.mipmap.aulas_indisponiveis_foreground));
            imagensCarousel.add(new CarouselPicker.DrawableItem(R.mipmap.aulas_indisponiveis_foreground));
            imagensCarousel.add(new CarouselPicker.DrawableItem(R.mipmap.aulas_indisponiveis_foreground));

            //floating action button
            floatingActionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    //redirecina para a Playlist
                    Toast.makeText(getApplicationContext(),"Aulas indisponíveis!", Toast.LENGTH_LONG).show();

                    floatingActionButton.animate();
                }
            });



        }else if(titulo.equals("FENÔMENOS MECÂNICOS")){

            collapsingToolbarLayout.setTitle("FEMEC");

            //mudando a cor da barrinha de cima
            collapsingToolbarLayout.setContentScrimColor(R.color.roxo);

            //Muda a cor do texto quando está expandido
            collapsingToolbarLayout.setExpandedTitleColor(R.color.colorText);

            //Muda a cor do texto quando fica na parte de cima, qnd damos scroll
            collapsingToolbarLayout.setCollapsedTitleTextColor(R.color.colorDark);

            ementa.setText("Fenômenos mecânicos a gente costuma chamar de “Femec”, aqui a gente revê conceitos de cinemática" +
                    " e dinâmica apresentados no ensino médio de maneira mais aprofundada e sistemática. Precisamos saber um" +
                    " pouco de cálculo pra poder cursar “Femec”. Aqui também é apresentada as principais leis de conservação da " +
                    "Física: conservação da energia e dos momentos linear e angular e suas aplicações. Assim como uma introdução " +
                    "às práticas experimentais da física envolvendo e exemplificando os conceitos apresentados na parte teórica do " +
                    "curso. É uma matéria com bastante coisa!\n");

            //Carousel1 e add suas imagens(no caso 3, pois definimos no xml que seriam 3, é o minimo)
            imagensCarousel.add(new CarouselPicker.DrawableItem(R.mipmap.fenomenos_mecanicos_foreground));
            imagensCarousel.add(new CarouselPicker.DrawableItem(R.mipmap.fenomenos_mecanicos_dois_foreground));
            imagensCarousel.add(new CarouselPicker.DrawableItem(R.mipmap.aulas_indisponiveis_foreground));

            floatingActionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    //redirecina para a Playlist
                    Toast.makeText(getApplicationContext(),"Bora pra playlist?!", Toast.LENGTH_LONG).show();

                    Uri uri = Uri.parse("https://youtube.com/playlist?list=PL9u-75TCsJ9VSZYsvJPRMJzp3ncUPkmXZ");
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                    floatingActionButton.animate();
                }
            });

        }




        CarouselPicker.CarouselViewAdapter imageAdapter = new CarouselPicker.CarouselViewAdapter(this, imagensCarousel,0);
        carouselPicker.setAdapter(imageAdapter);

        //Carousel2 e seus textos
    /*    List<CarouselPicker.PickerItem> textosCarousel2 = new ArrayList<>();
        textosCarousel2.add(new CarouselPicker.TextItem("Um", 20)); //20: textsize (dp)
        textosCarousel2.add(new CarouselPicker.TextItem("Dois", 20));
        textosCarousel2.add(new CarouselPicker.TextItem("Tres", 20));
        CarouselPicker.CarouselViewAdapter textoAdapter = new CarouselPicker.CarouselViewAdapter(this, textosCarousel2,0);
        carouselPicker2.setAdapter(textoAdapter);

        //Carousel3 e add suas imagens e textos
        List<CarouselPicker.PickerItem> textosEImagensCarousel3 = new ArrayList<>();
        textosEImagensCarousel3.add(new CarouselPicker.TextItem("Um", 20));
        textosEImagensCarousel3.add(new CarouselPicker.DrawableItem(R.mipmap.background_lgo_mulheres));
        textosEImagensCarousel3.add(new CarouselPicker.TextItem("Três", 20));
        textosEImagensCarousel3.add(new CarouselPicker.DrawableItem(R.mipmap.background_lgo_mulheres));
        CarouselPicker.CarouselViewAdapter textoEImagemAdapter = new CarouselPicker.CarouselViewAdapter(this, textosEImagensCarousel3,0);
        carouselPicker3.setAdapter(textoEImagemAdapter);
*/


    }
}
