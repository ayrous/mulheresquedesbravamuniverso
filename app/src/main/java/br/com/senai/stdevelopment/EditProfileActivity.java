package br.com.senai.stdevelopment;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
//import android.support.annotation.NonNull;
import androidx.annotation.NonNull;
//import android.support.v7.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;

public class EditProfileActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    private Button btnEditar;
    private ImageView imgProfileEdit;
    private TextView txtTrocaFoto;
    private EditText editUsername;
    private EditText editNomeCompleto;
    private Spinner spinnerAreas;
    private String areaDeAtuacao;


    private FirebaseUser usuarioFirebase;
    private Uri mImageUri;
    private StorageTask uploadTask;
    private StorageReference reference;
    private DatabaseReference databaseReference;

    private ProgressDialog loadingBar;

    private final static int GALERIA_PICKER = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        btnEditar = (Button) findViewById(R.id.btnEditarPerfil);
        editNomeCompleto = (EditText) findViewById(R.id.editNomeCompleto);
        editUsername = (EditText) findViewById(R.id.editNomeUsuario);
      //  areaAtuacao = (EditText) findViewById(R.id.editAreaAtuacao);
        txtTrocaFoto = (TextView) findViewById(R.id.textViewTrocaFoto);
        imgProfileEdit = (ImageView) findViewById(R.id.editImgPerfil);
        spinnerAreas = findViewById(R.id.spinnerAreasEditar);

        usuarioFirebase = FirebaseAuth.getInstance().getCurrentUser();
        reference = FirebaseStorage.getInstance().getReference().child("users").child("User");


        loadingBar = new ProgressDialog(this);

        //Config o spinner
        ArrayAdapter<CharSequence> adapterAreas = ArrayAdapter.createFromResource(this, R.array.areas, R.layout.support_simple_spinner_dropdown_item);
        adapterAreas.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinnerAreas.setAdapter(adapterAreas);
        spinnerAreas.setOnItemSelectedListener(this);


        databaseReference = FirebaseDatabase.getInstance().getReference().child("users").child("User").child(usuarioFirebase.getUid());
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String nomeUsuarioLogadoBanco = dataSnapshot.child("username").getValue().toString();
                String nomeCompletoUsuarioLogadoBanco = dataSnapshot.child("nomecompleto").getValue().toString();
                String usuarioFotoBanco = dataSnapshot.child("profileImg").getValue().toString();
                String areaAtuacaoBanco = dataSnapshot.child("areaatuacao").getValue().toString();

                editUsername.setText(nomeUsuarioLogadoBanco);
                editNomeCompleto.setText(nomeCompletoUsuarioLogadoBanco);
          //      areaAtuacao.setText(areaAtuacaoBanco);
                Picasso.with(EditProfileActivity.this).load(usuarioFotoBanco).placeholder(R.drawable.logofisica).into(imgProfileEdit);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        txtTrocaFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intentGaleria = new Intent();
                intentGaleria.setAction(Intent.ACTION_GET_CONTENT);
                intentGaleria.setType("image/*");
                startActivityForResult(intentGaleria,GALERIA_PICKER);

            }
        });


        //Quando clicar no botão Editar
        btnEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validacao();
            }
        });


    }


    private void validacao(){

        String userName = editUsername.getText().toString();
        String nomeCompleto = editNomeCompleto.getText().toString();
        String area = areaDeAtuacao;

        if(TextUtils.isEmpty(userName)){
            Toast.makeText(this, "Insira um nome de usuário!", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(nomeCompleto)) {

            Toast.makeText(this, "Nos diga seu nome completo!", Toast.LENGTH_SHORT).show();
        } else if(TextUtils.isEmpty(area)){
            Toast.makeText(this, "Adicione sua área de atuação!", Toast.LENGTH_SHORT).show();
        }else{
            loadingBar.setTitle("Atualizando seu perfil...");
            loadingBar.setMessage("Aguarda ai rapa");
            loadingBar.setCanceledOnTouchOutside(true);
            loadingBar.show();
            updateProfile(nomeCompleto,userName,area);
        }

    }

    //Método criado por mim -> edita as info e salva no banco
    private void updateProfile(String nomeCompleto, String username, String area) {
//        DatabaseReference reference = FirebaseDatabase.getInstance().getReference().child("users").child("User").child(usuarioFirebase.getUid());

        //Salvando efetivamente o user:
        HashMap usuarioMap = new HashMap();
        usuarioMap.put("username", username);
        usuarioMap.put("nomecompleto", nomeCompleto);
        usuarioMap.put("areaatuacao", area);
        uploadImage();
        databaseReference.updateChildren(usuarioMap).addOnCompleteListener(new OnCompleteListener() {
            @Override
            public void onComplete(@NonNull Task task) {

                if(task.isSuccessful()){
                    startActivity(new Intent(EditProfileActivity.this, MediaEntradaActivity.class));
                    finish();
                    Toast.makeText(EditProfileActivity.this, "Atualização realizada com Sucesso!", Toast.LENGTH_SHORT).show();
                    loadingBar.dismiss();
                }else{

                    Toast.makeText(EditProfileActivity.this, "Erro:", Toast.LENGTH_SHORT).show();
                    loadingBar.dismiss();
                }

            }
        });

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == GALERIA_PICKER && resultCode == RESULT_OK && data!=null){
            mImageUri = data.getData();
            imgProfileEdit.setImageURI(mImageUri);
        }else{
            Toast.makeText(this, "deu algo errado..", Toast.LENGTH_SHORT).show();
        }
    }


    //MÉTODO CRIADO POR MIM
    private String getFileExtension(Uri uri){
        ContentResolver contentResolver = getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();

        return  mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri));
    }

    //MÉTODO CRIADO POR MIM ->  sobe a imagem para firebase
    private void uploadImage(){
        loadingBar.setTitle("Atualizando...");
        loadingBar.setMessage("Aguarda ai rapa");
        loadingBar.setCanceledOnTouchOutside(true);
        loadingBar.show();

        if(mImageUri != null){
            final StorageReference fileReference = reference.child(System.currentTimeMillis()+"."+getFileExtension(mImageUri));

            // Comprime a imagem de perfil
            byte[] imgCompressaBytes = null;
            try {
                Bitmap bmpImg = MediaStore.Images.Media.getBitmap(getContentResolver(), mImageUri);
                ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
                bmpImg.compress(Bitmap.CompressFormat.JPEG, 50, byteStream);
                imgCompressaBytes = byteStream.toByteArray();
            } catch (IOException e) {
                Toast.makeText(this, "Ops! Algo aconteceu. " + e.toString(), Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
            // Salva a imagem compressa no Firebase
            uploadTask = fileReference.putBytes(imgCompressaBytes);
            uploadTask.continueWithTask(new Continuation() {
                @Override
                public Object then(@NonNull Task task) throws Exception {
                    if(!task.isSuccessful()){
                        String msg = task.getException().getMessage();
                        Toast.makeText(EditProfileActivity.this, "ERRO: "+msg, Toast.LENGTH_SHORT).show();
                        loadingBar.dismiss();
                    }
                return fileReference.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if(task.isSuccessful()){
                        Uri downloadUri = task.getResult();
                        String myUrl = downloadUri.toString();

                        DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference().child("users").child("User").child(usuarioFirebase.getUid());;

                        HashMap usuarioMap = new HashMap();
                        usuarioMap.put("profileImg", myUrl);
                        dbRef.updateChildren(usuarioMap);
                        loadingBar.dismiss();

                    }else{
                        String message = task.getException().getMessage();
                        Toast.makeText(EditProfileActivity.this, "Erro! " + message, Toast.LENGTH_SHORT).show();
                        loadingBar.dismiss();
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(EditProfileActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(this, "A imagem de perfil não foi alterada!", Toast.LENGTH_SHORT).show();
        }

    }

    //Métodos implementados para o Spinner áreas
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        String areaSelecionada = parent.getItemAtPosition(position).toString();
        Toast.makeText(this, areaSelecionada, Toast.LENGTH_SHORT).show();

        if(areaSelecionada == "Outro"){
        //    areaAtuacao.setVisibility(View.VISIBLE);
       //     areaDeAtuacao = areaAtuacao.getText().toString();
        }else{
            areaDeAtuacao = areaSelecionada;
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

}