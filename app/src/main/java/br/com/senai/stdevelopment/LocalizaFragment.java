package br.com.senai.stdevelopment;

import android.os.Bundle;
//import android.support.annotation.Nullable;
import androidx.annotation.Nullable;
//import android.support.v4.app.Fragment;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by Acer on 08/07/2020.
 */

public class LocalizaFragment extends Fragment {

    private WebView webView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_localiza, container, false);

        webView = (WebView) v.findViewById(R.id.webViewLocaliza);

        final WebViewClient client = new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url){
                // do your handling codes here, which url is the requested url
                // probably you need to open that url rather than redirect:
                view.loadUrl(url);
                return false; // then it is not handled by default action
            }

        };


        WebSettings webSettings = webView.getSettings();
        webSettings.setDomStorageEnabled(true);

        //usando url da internet
        webView.loadUrl("https://request500womenscientists.org/");
        webView.getSettings().setJavaScriptEnabled(true);

        return v;
    }
}
