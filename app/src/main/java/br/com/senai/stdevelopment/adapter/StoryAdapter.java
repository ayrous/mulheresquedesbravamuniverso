package br.com.senai.stdevelopment.adapter;

import android.content.Context;
//import android.support.v7.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.senai.stdevelopment.R;
import br.com.senai.stdevelopment.modelo.Posts;
import br.com.senai.stdevelopment.modelo.Story;

/**
 * Created by Acer on 05/08/2020.
 */

public class StoryAdapter extends RecyclerView.Adapter<StoryAdapter.ViewHolder>{

    //contexto
    private Context context;
    private List<Story> mStory;

    //Construtor
    public StoryAdapter(Context context, List<Story> mStory) {
        this.context = context;
        this.mStory = mStory;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if(viewType == 0){
            View v = LayoutInflater.from(context).inflate(R.layout.add_story_item, parent,false);
            return new StoryAdapter.ViewHolder(v);
        }else{
            View v = LayoutInflater.from(context).inflate(R.layout.story_item, parent,false);
            return new StoryAdapter.ViewHolder(v);
        }
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        Story story = mStory.get(position);
        usuarioInformacoes(holder,story.getUsuarioId(),position);

        if(holder.getAdapterPosition()!=0){
            storyVisto(holder,story.getStoryId());
        }

        if(holder.getAdapterPosition() ==0){
            myStory(holder.addStoryText, holder.addStory, false);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(holder.getAdapterPosition() == 0){
                    myStory(holder.addStoryText, holder.addStory,true);
                }else{
                    //ir para o storie
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return mStory.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public ImageView fotoStory, addStory, fotoVistaStory;
        public TextView usuarioStory,addStoryText;


        public ViewHolder(View itemView) {
            super(itemView);

            fotoStory = itemView.findViewById(R.id.fotoStory);
            addStoryText = itemView.findViewById(R.id.textViewAddStory);
            addStory = itemView.findViewById(R.id.addStory);
            fotoVistaStory = itemView.findViewById(R.id.story_foto_visto);
            usuarioStory = itemView.findViewById(R.id.storyUsername);



        }
    }

    @Override
    public int getItemViewType(int position) {

        if(position == 0){
            return 0;
        }
        return 1;

    }


    //FUNÇÃO CRIADA POR MIM -> CARREGA INFORMAÇÕES DO USUÁRIOS NA BOLINHA DOS STORIES
    private  void usuarioInformacoes(final ViewHolder viewHolder, final String usuarioId, final int posicao){

        DatabaseReference reference = FirebaseDatabase.getInstance().getReference().child("users").child("User").child(usuarioId);
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                //Setando o nome de usuario na barra de cima
                String nomeUsuarioLogado = dataSnapshot.child("username").getValue().toString();

                //Setando a foto de perfil
                String usuarioFotoBanco = dataSnapshot.child("profileImg").getValue().toString();
                Picasso.with(context).load(usuarioFotoBanco).into(viewHolder.fotoStory);

                if(posicao!= 0){
                    Picasso.with(context).load(usuarioFotoBanco).into(viewHolder.fotoVistaStory);
                    viewHolder.usuarioStory.setText(nomeUsuarioLogado);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }



    //MÉTODO FEITO POR MIM -> Verifica o horário da postagem do storie e a visibilidade dele
    private void myStory(final TextView textView, final ImageView imageView, final boolean click){

        DatabaseReference reference = FirebaseDatabase.getInstance().getReference().child("users").child("Story").child(FirebaseAuth.getInstance().getCurrentUser().getUid());;

        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                int count = 0;
                long timeAtual = System.currentTimeMillis();

                for(DataSnapshot dtSnap: dataSnapshot.getChildren()) {
                    Story story = dtSnap.getValue(Story.class);
                    if (timeAtual > story.getTimeStart() && timeAtual < story.getTimeEnd()) {
                        count++;
                    }
                }

                if(click){
                    //show alert
                    //mostrar que recurso estpa indisponível no momento
                    Toast.makeText(context, "Recurso indisponível!", Toast.LENGTH_SHORT).show();

                }else{
                    if(count>0){
                        Toast.makeText(context, "entreii", Toast.LENGTH_SHORT).show();
                        textView.setText("My Story");
                        imageView.setVisibility(View.GONE);
                    }else{
                        textView.setText("Add Story");
                        imageView.setVisibility(View.VISIBLE);
                    }
                }

            }


            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


    private void storyVisto(final ViewHolder v, String usuarioId){
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference().child("users").child("Story").child(usuarioId);

        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                int i = 0;

                for(DataSnapshot dtSnap : dataSnapshot.getChildren()){
                    if(! dtSnap.child("views").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).exists() && System.currentTimeMillis() < dtSnap.getValue(Story.class).getTimeEnd()){
                        i++;
                    }
                }

                if(i > 0){
                    v.fotoStory.setVisibility(View.VISIBLE);
                    v.fotoVistaStory.setVisibility(View.GONE);

                }else{
                    v.fotoStory.setVisibility(View.GONE);
                    v.fotoVistaStory.setVisibility(View.VISIBLE);

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

}
