package br.com.senai.stdevelopment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
//import android.support.annotation.NonNull;
import androidx.annotation.NonNull;
//import android.support.v7.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;

import java.util.HashMap;

public class TelaPrincipalActivity extends AppCompatActivity {

    private Button login;
    private EditText nome;
    private EditText senha;
    private Button cadastro;
    private FirebaseAuth auth;
    private ProgressDialog loading;
    private TextView esqueciSenha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_principal);

        //Background animado;
        animar();

        login = findViewById(R.id.btn_logar_id);
        nome = findViewById(R.id.edit_nome_id);
        senha = findViewById(R.id.edit_senha_id);
        cadastro = findViewById(R.id.btn_cadastrar_id);
        esqueciSenha = findViewById(R.id.lblEsqueci);

        loading = new ProgressDialog(this);

        auth = FirebaseAuth.getInstance();


        //Login Usuário;
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (nome.getText().length() == 0 && senha.getText().length() == 0) {
                    Toast.makeText(getApplicationContext(), "Insira seu nome de usuário e sua senha", Toast.LENGTH_LONG).show();

                } else if (nome.getText().length() == 0 || senha.getText().length() == 0) {
                    Toast.makeText(getApplicationContext(), "Insira seu nome de usuário e sua senha", Toast.LENGTH_LONG).show();

                } else if(nome.getText().length() != 0 && senha.getText().length() != 0) {

                    String n = nome.getText().toString();
                    String s = senha.getText().toString();

                    loading.setTitle("Logando");
                    loading.setMessage("aguarda ai rapa");
                    loading.show();
                    loading.setCanceledOnTouchOutside(true);


                    auth.signInWithEmailAndPassword(n, s).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {

                            if(task.isSuccessful()){

                                Intent intent = new Intent(TelaPrincipalActivity.this, SobreNosActivity.class);
                                intent.putExtra("nome", nome.getText().toString());
                                finish();
                                startActivity(intent);

                                Toast.makeText(TelaPrincipalActivity.this, "sucesso!", Toast.LENGTH_SHORT).show();
                            }else{
                                String msg = task.getException().getMessage();
                                Toast.makeText(TelaPrincipalActivity.this, "Ops: "+msg, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                }
            }
        });

        //Esqueci Senha
        esqueciSenha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialogo().criarAlertEsqueciSenha(TelaPrincipalActivity.this);
                //Toast.makeText(TelaPrincipalActivity.this, "Cliquei no botão Esqueci Senha!", Toast.LENGTH_SHORT).show();

            }
        });



        //Criar novo usuário
        cadastro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), CriarUsuarioActivity.class);
//                finish();
                startActivity(intent);

                //Toast.makeText(TelaPrincipalActivity.this, "Cliquei no botão Cadastrar!", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void animar(){
        RelativeLayout relativeLayout = findViewById(R.id.meuLayout);
        AnimationDrawable animationDrawable = (AnimationDrawable) relativeLayout.getBackground();
        animationDrawable.setEnterFadeDuration(3000);
        animationDrawable.setExitFadeDuration(5000);
        animationDrawable.start();
    }


    @Override
    protected void onStart() {
        super.onStart();

        FirebaseUser firebaseUser = auth.getCurrentUser();

        if(firebaseUser != null){
            Intent intent = new Intent(TelaPrincipalActivity.this, SobreNosActivity.class);
            finish();
            startActivity(intent);
        }

    }
}