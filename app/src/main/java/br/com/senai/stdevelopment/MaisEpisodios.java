package br.com.senai.stdevelopment;

import android.os.Bundle;
//import android.support.annotation.Nullable;
import androidx.annotation.Nullable;
//import android.support.v4.view.ViewPager;
import androidx.viewpager.widget.ViewPager;
//import android.support.v7.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.Toast;

import br.com.senai.stdevelopment.PodcastActivity;
import br.com.senai.stdevelopment.R;
import br.com.senai.stdevelopment.adapter.MaisEpisodiosAdapter;
import br.com.senai.stdevelopment.adapter.SliderAdapter;

/**
 * Created by Acer on 31/12/2020.
 */

public class MaisEpisodios extends AppCompatActivity{

    private ViewPager mSliderViewPager;

    private ViewPager sliderViewPager;

    private MaisEpisodiosAdapter maisEpAdapter;

    private SliderAdapter sliderAdapter;

    private int paginaAtual;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.maisepisodios);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int)(width*.8),(int)(height*.6));

        maisEpAdapter = new MaisEpisodiosAdapter(this);

        mSliderViewPager = (ViewPager) findViewById(R.id.sliderViewPopup);
        mSliderViewPager.setAdapter(maisEpAdapter);
        mSliderViewPager.addOnPageChangeListener(viewListener);

        Bundle extras = getIntent().getExtras();
        paginaAtual = extras.getInt("posicaoSlider");

        mSliderViewPager.setCurrentItem(paginaAtual);


    }

    //indicar as mudanças dos pontinhos(que nn vao ter aqui)
    ViewPager.OnPageChangeListener viewListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            position = paginaAtual;
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };
}