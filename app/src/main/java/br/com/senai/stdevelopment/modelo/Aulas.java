package br.com.senai.stdevelopment.modelo;

import java.io.Serializable;

/**
 * Created by Acer on 02/07/2020.
 */

public class Aulas implements Serializable {

    private Long id;
    private String titulo;


    @Override
    public String toString() {
        return getId() + "\n" + "\t\t\t" + getTitulo();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
}
