package br.com.senai.stdevelopment;

//import android.support.v7.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.TextView;

public class ProjetosBrasilActivity extends AppCompatActivity {

    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_projetos_brasil);

        //usando html local meu
        webView = (WebView) findViewById(R.id.webViewProjetos2);
        webView.loadUrl("file:///android_asset/projetoBrasil.html");
        webView.getSettings().setJavaScriptEnabled(true);


    }
}
