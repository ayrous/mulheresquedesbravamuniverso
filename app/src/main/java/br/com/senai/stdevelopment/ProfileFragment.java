package br.com.senai.stdevelopment;

import android.content.Intent;
//import android.support.annotation.Nullable;
import androidx.annotation.Nullable;
//import android.support.design.widget.FloatingActionButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
//import android.support.v4.app.Fragment;
import androidx.fragment.app.Fragment;
//import android.support.v7.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class ProfileFragment extends Fragment {


    private  FirebaseAuth userAuth;
    private DatabaseReference databaseReference;

    private String usuarioId;
    private String nomeUsuarioLogado;

    private TextView usernameBarra;
    private TextView nomeCompleto;
    private TextView username;
    private ImageView imagemPerfil;
    private TextView areaAtuacao;
    private FloatingActionButton btnEditar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_profile, container, false);

        userAuth = FirebaseAuth.getInstance();
        usuarioId = userAuth.getCurrentUser().getUid();
        databaseReference = FirebaseDatabase.getInstance().getReference().child("users").child("User");
        usernameBarra = v.findViewById(R.id.textViewUserName);
        nomeCompleto = v.findViewById(R.id.textNomeCompleto);
        username = v.findViewById(R.id.textViewNomeUsuario);
        imagemPerfil = v.findViewById(R.id.imgPerfil);
        areaAtuacao = v.findViewById(R.id.textViewAreaAtuacao);
        btnEditar = v.findViewById(R.id.floatingEditaPerfil);


        btnEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),EditProfileActivity.class);
                startActivity(intent);
            }
        });

        databaseReference.child(usuarioId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if(dataSnapshot.exists()){

                    //Setando o nome de usuario na barra de cima
                    nomeUsuarioLogado = dataSnapshot.child("username").getValue().toString();
                    usernameBarra.setText("DDU • "+nomeUsuarioLogado);

                    //Setando o nome de usuario
                    username.setText("@"+nomeUsuarioLogado);

                    //Setando o nome completo
                    String nomeCompletoBanco = dataSnapshot.child("nomecompleto").getValue().toString();
                    nomeCompleto.setText(nomeCompletoBanco);

                    //Setando a foto de perfil
                    String usuarioFotoBanco = dataSnapshot.child("profileImg").getValue().toString();
                    Picasso.with(getActivity()).load(usuarioFotoBanco).into(imagemPerfil);

                    //Setando a area de atuação
                    String areaAtuacaoBanco = dataSnapshot.child("areaatuacao").getValue().toString();
                    areaAtuacao.setText(areaAtuacaoBanco);

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        return v;

    }


}