package br.com.senai.stdevelopment;

import android.app.ProgressDialog;
import android.content.Intent;
//import android.support.annotation.NonNull;
import androidx.annotation.NonNull;
//import android.support.v7.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.senai.stdevelopment.modelo.PostsTags;

public class TesteActivity extends AppCompatActivity {


    private FirebaseAuth firebaseAuth;
    private DatabaseReference databaseReference;
    private String usuarioId;

    private Button btnProximo;

    private boolean matematica;
    private boolean fisica;
    private boolean biologia;
    private boolean quimica;
    private boolean historia;
    private boolean geografia;
    private boolean filosofia;
    private boolean sociologia;
    private boolean programacao;
    private boolean politica;

    private CheckBox checkFilosofia;
    private CheckBox checkSociologia;
    private CheckBox checkQuimica;
    private CheckBox checkBiologia;
    private CheckBox checkFisica;
    private CheckBox checkMatematica;
    private CheckBox checkPolitica;
    private CheckBox checkProgramacao;
    private CheckBox checkHistoria;
    private CheckBox checkGeografia;


    private ProgressDialog loading;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teste);

        btnProximo = (Button) findViewById(R.id.btnProximo);

        firebaseAuth = FirebaseAuth.getInstance();
        usuarioId = firebaseAuth.getCurrentUser().getUid();
        databaseReference = FirebaseDatabase.getInstance().getReference().child("users").child("User").child(usuarioId);


        checkFilosofia = (CheckBox) findViewById(R.id.checkBoxFilosofia);
        checkSociologia = (CheckBox) findViewById(R.id.checkBoxSociologia);
        checkBiologia = (CheckBox) findViewById(R.id.checkBoxBiologia);
        checkQuimica = (CheckBox) findViewById(R.id.checkBoxQuimica);
        checkFisica = (CheckBox) findViewById(R.id.checkBoxFisica);
        checkMatematica = (CheckBox) findViewById(R.id.checkBoxMatematica);
        checkHistoria = (CheckBox) findViewById(R.id.checkBoxHistoria);
        checkGeografia = (CheckBox) findViewById(R.id.checkBoxGeografia);
        checkProgramacao = (CheckBox) findViewById(R.id.checkBoxProgramacao);
        checkPolitica = (CheckBox) findViewById(R.id.checkBoxPolitica);


        loading = new ProgressDialog(this);


        btnProximo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkFisica.isChecked()){
                    fisica = true;
                    Toast.makeText(TesteActivity.this, "clicou", Toast.LENGTH_SHORT).show();
                } else{
                    fisica = false;
                }

                if (checkGeografia.isChecked()){
                    geografia = true;
                } else{
                    geografia = false;
                }


                if (checkHistoria.isChecked()){
                    historia = true;
                } else{
                    historia = false;
                }

                if(checkBiologia.isChecked()){
                    biologia = true;
                }else{
                    biologia = false;
                }


                if(checkQuimica.isChecked()){
                    quimica = true;
                }else{
                    quimica = false;
                }


                if(checkFilosofia.isChecked()){
                    filosofia = true;
                }else {
                    filosofia = false;
                }


                if(checkSociologia.isChecked()){
                    sociologia = true;
                }else {
                    sociologia = false;
                }

                if(checkProgramacao.isChecked()){
                    programacao = true;
                }else {
                    programacao = false;
                }

                if(checkMatematica.isChecked()){
                    matematica = true;
                }else {
                    matematica = false;
                }

                if(checkPolitica.isChecked()){
                    politica = true;
                }else {
                    politica = false;
                }

                salvarInformacoesContaMidias();
            }
        });
    }

    private void salvarInformacoesContaMidias() {

        //Salvando os likes do usuario
        Map<String, Boolean> Likes = new HashMap<>();
        HashMap usuarioMap = new HashMap();

        Likes.put("filosofia", filosofia);
        Likes.put("sociologia", sociologia);
        Likes.put("quimica", quimica);
        Likes.put("biologia", biologia);
        Likes.put("fisica", fisica);
        Likes.put("matematica", matematica);
        Likes.put("historia", historia);
        Likes.put("geografia", geografia);
        Likes.put("programacao", programacao);
        Likes.put("politica", politica);

        usuarioMap.put("Likes",Likes);

        databaseReference.updateChildren(usuarioMap).addOnCompleteListener(new OnCompleteListener() {
            @Override
            public void onComplete(@NonNull Task task) {

                if (task.isSuccessful()) {
                    Intent intent = new Intent(TesteActivity.this, SobreNosActivity.class);
                    startActivity(intent);
                    finish();

                    Toast.makeText(TesteActivity.this, "Cadastro finalizado!", Toast.LENGTH_LONG).show();
                    loading.dismiss();
                } else {

                    String msg = task.getException().getMessage();
                    Toast.makeText(TesteActivity.this, "Ops... " + msg, Toast.LENGTH_SHORT).show();
                    loading.dismiss();
                }
            }
        });


    }


    //Verifica qual checkbox foi selecionado
    public void onCheckboxClicked(View view) {
        // Is the view now checked?
//        boolean checked = ((CheckBox) view).isChecked();

                if (checkFisica.isChecked()){
                    fisica = true;
                    Toast.makeText(this, "clicou", Toast.LENGTH_SHORT).show();
                } else{
                    fisica = false;
                }

                if (checkGeografia.isChecked()){
                    geografia = true;
                } else{
                    geografia = false;
                }


                if (checkHistoria.isChecked()){
                    historia = true;
                } else{
                    historia = false;
                }

                if(checkBiologia.isChecked()){
                    biologia = true;
                }else{
                    biologia = false;
                }


                if(checkQuimica.isChecked()){
                    quimica = true;
                }else{
                    quimica = false;
                }


                if(checkFilosofia.isChecked()){
                    filosofia = true;
                }else {
                    filosofia = false;
                }


                if(checkSociologia.isChecked()){
                    sociologia = true;
                }else {
                    sociologia = false;
                }

                if(checkProgramacao.isChecked()){
                    programacao = true;
                }else {
                    programacao = false;
                }

                if(checkMatematica.isChecked()){
                    matematica = true;
                }else {
                    matematica = false;
                }


                // TODO: verifica opções escolhidas
        }
    }

