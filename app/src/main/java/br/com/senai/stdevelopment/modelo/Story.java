package br.com.senai.stdevelopment.modelo;

/**
 * Created by Acer on 05/08/2020.
 */

public class Story {

    //atributos
    private String urlImagem;
    private long timeStart;
    private long timeEnd;
    private String storyId;
    private String usuarioId;

    public Story(String urlImagem, long timeStart, long timeEnd, String storyId, String usuarioId) {
        this.urlImagem = urlImagem;
        this.timeStart = timeStart;
        this.timeEnd = timeEnd;
        this.storyId = storyId;
        this.usuarioId = usuarioId;
    }


    public String getUrlImagem() {
        return urlImagem;
    }

    public void setUrlImagem(String urlImagem) {
        this.urlImagem = urlImagem;
    }

    public long getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(long timeStart) {
        this.timeStart = timeStart;
    }

    public long getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(long timeEnd) {
        this.timeEnd = timeEnd;
    }

    public String getStoryId() {
        return storyId;
    }

    public void setStoryId(String storyId) {
        this.storyId = storyId;
    }

    public String getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(String usuarioId) {
        this.usuarioId = usuarioId;
    }


    public Story() {
    }
}
