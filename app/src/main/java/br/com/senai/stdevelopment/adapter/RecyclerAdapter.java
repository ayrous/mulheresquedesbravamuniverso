package br.com.senai.stdevelopment.adapter;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import androidx.annotation.RequiresApi;

//import android.support.v7.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView;

import android.transition.Explode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import br.com.senai.stdevelopment.AulasActivity;
import br.com.senai.stdevelopment.CardItem;
import br.com.senai.stdevelopment.ConteudoAulaActivity;
import br.com.senai.stdevelopment.R;
import br.com.senai.stdevelopment.modelo.Aulas;

/**
 * Created by Acer on 01/07/2020.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.myViewHolder> {



    Context context;
    List<CardItem> cardItens;
    private OnNoteListener mOnNoteListener;
    //byte[] bytes;



    //construtor
    public RecyclerAdapter(Context context, List<CardItem> cardItens, OnNoteListener onNoteListener) {
        this.context = context;
        this.cardItens = cardItens;
        this.mOnNoteListener = onNoteListener;
    }


    //Métodos obrigatórios implementados
    @Override
    public myViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View v = layoutInflater.inflate(R.layout.card_item,parent,false);

        return new myViewHolder(v, mOnNoteListener);
    }



    //FAZER ADAPTER PARA O CCONTEUDO DAS AULAS!!!
    @Override
    public void onBindViewHolder(myViewHolder holder, int position) {

        holder.backgroundImg.setImageResource(cardItens.get(position).getBackground());
        holder.imagemAula.setImageResource(cardItens.get(position).getAula_imagem());
        holder.tituloAula.setText(cardItens.get(position).getNome_aulas());
        holder.acessoAula.setText(cardItens.get(position).getAcessos_aula()+" vídeos");


        //BitmapDrawable bitmapDrawable = (BitmapDrawable) holder.backgroundImg.getDrawable();
        //get image from drawble
    //    Bitmap bitmap = bitmapDrawable.getBitmap();
      //  ByteArrayOutputStream stream =  new ByteArrayOutputStream();

        //compress image
  //      bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);

        //convert to byte array
//        bytes = stream.toByteArray();

        //Intent intent = new Intent(context, ConteudoAulaActivity.class);
        //intent.putExtra("imagem", bytes);
        //Toast.makeText(context, "passsseiiii", Toast.LENGTH_SHORT).show();
        //context.startActivity(intent);


    }

    @Override
    public int getItemCount() {
        return cardItens.size();
    }




    //Minha classe da view
    public class myViewHolder extends  RecyclerView.ViewHolder implements View.OnClickListener{

        ImageView imagemAula, backgroundImg;
//        VideoView backVideoView;
        TextView tituloAula, acessoAula;
        Button btnAssistir;
        OnNoteListener onNoteListener;


        //constructor
        public myViewHolder(View itemView, OnNoteListener onNoteListener) {
            super(itemView);

            imagemAula = itemView.findViewById(R.id.aula_tema);
            backgroundImg = itemView.findViewById(R.id.backgroundCard);
            tituloAula = itemView.findViewById(R.id.tituloVideoAula);
            acessoAula = itemView.findViewById(R.id.acessosVideoAula);
            btnAssistir = itemView.findViewById(R.id.btnAssistir);

            this.onNoteListener = onNoteListener;
//            btnAssistir = itemView.findViewById(R.id.btnAssistir);
            btnAssistir.setOnClickListener(this);

        }

        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void onClick(View v) {
            onNoteListener.OnNoteClick(getAdapterPosition());

            //Passando dados para a nova activity:
            String titulo = cardItens.get(getAdapterPosition()).getNome_aulas();

            //pegando a img de fundo
            int imagem = cardItens.get(getAdapterPosition()).getBackground();

            // Mostrando msg
            //Toast.makeText(context, "aqui:"+titulo, Toast.LENGTH_SHORT).show();

            //chamando Intent pelo context(obs da pra zr isso tbm no AulasActivity em OnNoteClick)
            Intent intent = new Intent(context, ConteudoAulaActivity.class);
            intent.putExtra("titulo", titulo);
            intent.putExtra("img", imagem);

            //animação entre activities
//            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation((Activity) context, v.findViewById(R.id.), "robot");
//            context.startActivity(intent, options.toBundle());
            context.startActivity(intent);


        }
    }

    public interface OnNoteListener{
        void OnNoteClick(int position);


        }


        public  void filter(ArrayList<CardItem> c){
            cardItens = c;
            notifyDataSetChanged();
        }
}
