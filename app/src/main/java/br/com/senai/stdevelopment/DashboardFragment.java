package br.com.senai.stdevelopment;

import android.app.Dialog;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
//import android.support.annotation.Nullable;
import androidx.annotation.Nullable;
//import android.support.v4.app.Fragment;
import androidx.fragment.app.Fragment;
//import android.support.v7.widget.CardView;
import androidx.cardview.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

/**
 * Created by Acer on 08/07/2020.
 */

public class DashboardFragment extends Fragment {

    private CardView cardDados;
    private CardView cardNews;
    private CardView cardProjetosBrasil;

    //Popup
    private Dialog popup;


    //Para mostrar dados do usuário
    private  FirebaseAuth userAuth;
    private DatabaseReference databaseReference;

    private String usuarioId;
    private String nomeUsuarioLogado;

    private ImageView imagemPerfil;
    private TextView textNome;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_dashboard, container, false);


        imagemPerfil = (ImageView) v.findViewById(R.id.imagemDashBoard);
        textNome = (TextView) v.findViewById(R.id.subtituloDashboard);

        userAuth = FirebaseAuth.getInstance();
        usuarioId = userAuth.getCurrentUser().getUid();
        databaseReference = FirebaseDatabase.getInstance().getReference().child("users").child("User");


        cardDados = v.findViewById(R.id.cardView01);
        cardNews = (CardView) v.findViewById(R.id.cardView02);
        cardProjetosBrasil = v.findViewById(R.id.cardView03);


        //tela de popup
        popup = new Dialog(v.getContext());


        cardDados.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "clicou dados", Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(v.getContext(), DadosActivity.class);
                startActivity(intent);
            }
        });

        cardNews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "clicou news", Toast.LENGTH_SHORT).show();
                showPopupNews(v);
            }
        });


        cardProjetosBrasil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "clicou projetos", Toast.LENGTH_SHORT).show();
              //  showPopupProjetos(v);
                Intent intent = new Intent(v.getContext(), ProjetosBrasilActivity.class);
                startActivity(intent);
            }
        });



        databaseReference.child(usuarioId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if(dataSnapshot.exists()){
                    nomeUsuarioLogado = dataSnapshot.child("username").getValue().toString();
                    String usuarioFoto = dataSnapshot.child("profileImg").getValue().toString();

                    textNome.setText("  @"+nomeUsuarioLogado);
                    Picasso.with(getActivity()).load(usuarioFoto).into(imagemPerfil);

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });




        return v;
    }


    //MINHA FUNÇÃO -> Mostra popup news
    public  void showPopupNews(View v){

        TextView fechar;
        WebView webView;


        popup.setContentView(R.layout.popup_news);
        fechar = (TextView) popup.findViewById(R.id.textViewPopupNews);
        webView = (WebView) popup.findViewById(R.id.webViewNews);

        //usando url da internet
        webView.loadUrl("https://www.bbc.com/portuguese/topics/c340q4kdy9kt");
        webView.getSettings().setJavaScriptEnabled(true);

        fechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });

        popup.show();

    }


    //MINHA FUNÇÃO -> Mostra popup projetos
    public  void showPopupProjetos(View v){

        TextView fechar;
        WebView webView;

        //usando html local meu
        popup.setContentView(R.layout.popup_projetos_brasil);
        fechar = (TextView) popup.findViewById(R.id.textViewPopupProjetos);
        webView = (WebView) popup.findViewById(R.id.webViewProjetos);
        webView.loadUrl("file:///android_asset/projetoBrasil.html");
        webView.getSettings().setJavaScriptEnabled(true);

        fechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });

        popup.show();

    }


}
