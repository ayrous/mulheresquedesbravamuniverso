package br.com.senai.stdevelopment.modelo;

/**
 * Created by Acer on 02/09/2020.
 */

public enum PostsTags {

    FILOSOFIA,
    SOCIOLOGIA,
    MATEMATICA,
    QUIMICA,
    FISICA,
    PROGRAMACAO,
    HISTORIA,
    GEOGRAFIA,
    BIOLOGIA;

}
