package br.com.senai.stdevelopment;

import android.os.Build;
import androidx.annotation.RequiresApi;
//import android.support.v7.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
//import android.support.v7.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.senai.stdevelopment.adapter.RecyclerAdapter;

public class AulasActivity extends AppCompatActivity implements RecyclerAdapter.OnNoteListener{


    private static  final String TAG = "AulasActivity";

    private Button assistirAula;
    private TextView titulo;
    List<CardItem> cardItems = new ArrayList<>();
    private boolean clicou = false;

    RecyclerAdapter recyclerAdapter;

    //parte da pesquisa(search)
    private EditText buscaDisciplina;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aulas);
        // inside your activity (if you did not enable transitions in your theme)
  //      getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);

        // set an exit transition
    //    getWindow().setExitTransition(new Explode());


        titulo = findViewById(R.id.tituloVideoAula);
        buscaDisciplina = findViewById(R.id.editTextBuscaMateria);

        //mudar o estado da cor de fundo da barrinha de cima do App para transparente
        Window w = getWindow();
        w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        //setup recyclerview e adapter
        RecyclerView recyclerView = findViewById(R.id.recyclerView);

        //imagem de gundo do item,t titulo da matéria, fotinho da pessoa logada e quantos videos têm
        cardItems.add(new CardItem(R.drawable.bases_matematicas_capatres,"BASES MATEMÁTICAS", R.drawable.users_barbara, 2));
        cardItems.add(new CardItem(R.drawable.estrutura_materia_capa,"ESTRUTURA DA MATÉRIA", R.drawable.users_barbara, 2));
        cardItems.add(new CardItem(R.drawable.fundo_bcc, "BASES COMPUTACIONAIS DA CIÊNCIA", R.drawable.login, 0));
        cardItems.add(new CardItem(R.drawable.aulas_indisponiveis_capa, "BASES EXPERIMENTAIS DAS CIÊNCIAS NATURAIS", R.drawable.login, 0));
        cardItems.add(new CardItem(R.drawable.fenomenos_mecanicos_capa, "FENÔMENOS MECÂNICOS", R.drawable.users_barbara, 2));

        recyclerAdapter = new RecyclerAdapter(this, cardItems, this);
        recyclerView.setAdapter(recyclerAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


        //Busca por itens:
        buscaDisciplina.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                filter(s.toString());

            }
        });

    }


    //Método criado PO MIM - fzr a pesquisa de disciplinas
    private void filter(String textoProcurado) {

        ArrayList<CardItem> listaDisciplinasAchadas = new ArrayList<>();

        for(CardItem c: cardItems){
            if(c.getNome_aulas().toLowerCase().contains(textoProcurado.toLowerCase())){
                listaDisciplinasAchadas.add(c);
            }
        }

        recyclerAdapter.filter(listaDisciplinasAchadas);

    }

    @Override
    public void OnNoteClick(int position) {

        //OBS: o codigo abaixo funfa, porém n sei pq n passou os extras, ai fiz tudo no recyclerAdapter

//        cardItems.get(position);
 //       CardItem cardItem = (CardItem) cardItems.get(position);
   //     Toast.makeText(this, "Passei:"+ cardItem.getNome_aulas().toString(), Toast.LENGTH_SHORT).show();

//        Intent intent = new Intent(this, ConteudoAulaActivity.class);
  //      startActivity(intent,
    //            ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
    }
}
