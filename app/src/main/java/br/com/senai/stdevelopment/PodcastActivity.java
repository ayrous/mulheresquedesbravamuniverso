package br.com.senai.stdevelopment;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
//import android.support.v4.view.ViewPager;
import androidx.viewpager.widget.ViewPager;
//import android.support.v7.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import br.com.senai.stdevelopment.adapter.SliderAdapter;

public class PodcastActivity extends AppCompatActivity {

    private ViewPager mSliderViewPager;
    private LinearLayout mDotLayout;

    private SliderAdapter sliderAdapter;

    private TextView[] mDots;

    //Botoes
    private Button btnProximo;
    private Button btnAnterior;
    private int paginaAtual;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_podcast);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

//        paginaAtual = paginaAtual + 1;

        btnAnterior = (Button) findViewById(R.id.btnAntes);
        btnProximo = (Button) findViewById(R.id.btnProximo);

        mSliderViewPager = (ViewPager) findViewById(R.id.sliderView);
        mDotLayout = (LinearLayout) findViewById(R.id.linearLayout);

        sliderAdapter = new SliderAdapter(this);

        mSliderViewPager.setAdapter(sliderAdapter);

        addDotsIndicator(0);

        mSliderViewPager.addOnPageChangeListener(viewListener);


        //onclick - botoes de proximo e anterior

        btnAnterior.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSliderViewPager.setCurrentItem(paginaAtual-1);
            }
        });

        btnProximo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSliderViewPager.setCurrentItem(paginaAtual+1);
                }
            });

    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void addDotsIndicator(int position) {

        mDots = new TextView[4];
        mDotLayout.removeAllViews();

        for (int i = 0; i < mDots.length; i++) {

            mDots[i] = new TextView(this);
            mDots[i].setText(Html.fromHtml("&#8226;"));
            mDots[i].setTextSize(35);
            mDots[i].setTextColor(getResources().getColor(R.color.colorTransparenteWhite));
            mDots[i].setTextAlignment(View.TEXT_ALIGNMENT_CENTER);


            mDotLayout.addView(mDots[i]);
        }

        if(mDots.length > 0){
                mDots[position].setTextColor(getResources().getColor(R.color.colorWhite));
            }
        }



    //indicar as mudanças dos pontinhos
    ViewPager.OnPageChangeListener viewListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            addDotsIndicator(position);
            paginaAtual = position;

//            Toast.makeText(getApplicationContext(), "dot:!"+position, Toast.LENGTH_LONG).show();


            if(position == 0){

             //   Toast.makeText(getApplicationContext(), "if1!", Toast.LENGTH_LONG).show();

                btnProximo.setEnabled(true);
                btnAnterior.setEnabled(false);
                btnAnterior.setVisibility(View.INVISIBLE);

                btnProximo.setText("PROXIMO");
                btnAnterior.setText("");
                btnProximo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mSliderViewPager.setCurrentItem(paginaAtual+1);
                    }
                });

            } else if(position == mDots.length-1){
               // Toast.makeText(getApplicationContext(), "if 2!", Toast.LENGTH_LONG).show();

                btnAnterior.setEnabled(true);
                btnAnterior.setVisibility(View.VISIBLE);

                btnProximo.setText("FINALIZAR");

                btnAnterior.setText("ANTERIOR");

                btnProximo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(PodcastActivity.this, SobreNosActivity.class);
                        finish();
                        startActivity(intent);
                    }
                });

            }else{
                //Toast.makeText(getApplicationContext(), "if 3!", Toast.LENGTH_LONG).show();

                btnProximo.setEnabled(true);
                btnAnterior.setEnabled(true);
                btnAnterior.setVisibility(View.VISIBLE);
//                btnProximo.setVisibility(View.INVISIBLE);

                btnProximo.setText("PROXIMO");
                btnAnterior.setText("ANTERIOR");

                btnProximo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mSliderViewPager.setCurrentItem(paginaAtual+1);
                    }
                });


            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };
}