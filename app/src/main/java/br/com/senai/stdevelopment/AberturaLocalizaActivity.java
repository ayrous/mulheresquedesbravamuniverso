package br.com.senai.stdevelopment;

import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
//import androidx.support.v4.app.Fragment;
//import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;




public class AberturaLocalizaActivity extends Fragment {


    private Fragment fragment;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.activity_abertura_localiza, container, false);

        //configurações de transição (não deixar em fullscreen)
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //determinar a duração da tela, no caso 5 segundos
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                //Depois dos 5 segundos configuradoa abaixo, vamos redirecionar para o fragment Localiza
                Intent intent = new Intent();
                intent.setClass(getActivity(), LocalizaFragment.class);
//                intent.putExtra("index", index);
                startActivity(intent);

            }
        }, 5000);


        return v;
    }

}