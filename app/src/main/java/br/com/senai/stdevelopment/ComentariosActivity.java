package br.com.senai.stdevelopment;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import br.com.senai.stdevelopment.modelo.Comentarios;
import br.com.senai.stdevelopment.modelo.Posts;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class ComentariosActivity extends AppCompatActivity {

    private RecyclerView listaComentarios;
    private TextView textDesc;
    private TextView textUser;
    private ImageView imgProfileUsuario, btnEnviarComentario;
    private EditText editComentario;
    private String postKey, usuarioFotoPost, usuarioNome, descPost;

    private FirebaseAuth usuarioLogado;
    private String usuarioLogadoId;
    private DatabaseReference usuariosRef, postComentariosRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Remove o título da janela
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_comentarios);

        textDesc = (TextView) findViewById(R.id.textComentarios);
        textUser = (TextView) findViewById(R.id.textUsuarioPost);
        imgProfileUsuario = (ImageView) findViewById(R.id.profileImageComentario);
        btnEnviarComentario = (ImageView) findViewById(R.id.enviarComentario);
        editComentario = (EditText) findViewById(R.id.editComentarios);

        listaComentarios = (RecyclerView) findViewById(R.id.recyclerComentarios1);
        listaComentarios.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        listaComentarios.setLayoutManager(linearLayoutManager);

        postKey = getIntent().getExtras().get("postKey").toString();
        usuarioFotoPost = getIntent().getExtras().get("usuarioFoto").toString();
        usuarioNome = getIntent().getExtras().get("usuarioNome").toString();
        descPost = getIntent().getExtras().get("descricao").toString();

        usuarioLogado = FirebaseAuth.getInstance();
        usuarioLogadoId = usuarioLogado.getCurrentUser().getUid();
        usuariosRef = FirebaseDatabase.getInstance().getReference().child("users").child("User");
        postComentariosRef = FirebaseDatabase.getInstance().getReference().child("users").child("Post").child(postKey).child("comentarios");


        Picasso.with(ComentariosActivity.this).load(usuarioFotoPost).into(imgProfileUsuario);
        textUser.setText("@"+usuarioNome);
        textDesc.setText(descPost);
        textUser.setPaintFlags(textUser.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);


        //ao clicar em enviar o comentário
        btnEnviarComentario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                usuariosRef.child(usuarioLogadoId).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        if(dataSnapshot.exists()){
                            String username = dataSnapshot.child("username").getValue().toString();
                            validarComentario(username);
                            editComentario.setText("");
                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        });



    }

    @Override
    protected void onStart() {
        super.onStart();
        Query queryOrdenarComentariosData = postComentariosRef.orderByChild("data");

        FirebaseRecyclerOptions<Comentarios> comentariosAdapterOptions =
                new FirebaseRecyclerOptions.Builder<Comentarios>()
                        .setQuery(queryOrdenarComentariosData, Comentarios.class)
                        .setLifecycleOwner(this)
                        .build();

        FirebaseRecyclerAdapter comentariosRecyclerAdapter = new FirebaseRecyclerAdapter<Comentarios, ComentariosViewHolder>(comentariosAdapterOptions){
            @Override
            public ComentariosViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                //Infla o layout dos comentários para que ele possa ser populado no onBindViewHolder
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.display_todos_comentarios, parent, false);

                return new ComentariosActivity.ComentariosViewHolder(view);
            }

            //Nota: esta função exerce a mesma função da populateViewHolder (depreciada com a atualização da FirebaseUI)
            @Override
            protected void onBindViewHolder(ComentariosViewHolder comentariosViewHolder, int i, Comentarios comentarios) {
                comentariosViewHolder.setComentario(comentarios.getComentario());
                String comentarioData = comentarios.getData().replaceAll("-","/");
                comentariosViewHolder.setData(comentarioData);
                comentariosViewHolder.setHora(comentarios.getHora());
                comentariosViewHolder.setUsername(comentarios.getUsername());

                Toast.makeText(ComentariosActivity.this, "ussername:"+comentarios.getUsername(), Toast.LENGTH_SHORT).show();
            }
        };

        listaComentarios.setAdapter(comentariosRecyclerAdapter);
    }

    public static  class ComentariosViewHolder extends RecyclerView.ViewHolder{

        View mView;

        public ComentariosViewHolder(@NonNull View itemView) {
            super(itemView);
            mView = itemView;
        }

        public void setComentario(String comentario) {

            TextView comment = mView.findViewById(R.id.txtViewComentario);
            comment.setText(comentario);
        }

        public void setData(String data){

            TextView dataComm = mView.findViewById(R.id.txtDataComentario);
            dataComm.setText("  "+data);

        }
        public void setHora(String hora){

            TextView horaComm = mView.findViewById(R.id.txtViewHoraComentario);
            horaComm.setText("   "+hora);

        }

        public void setUsername(String username){

            TextView myUsername = mView.findViewById(R.id.txtUsernameComment);
            myUsername.setText("@"+username+"  ");

        }
    }



    //FUNÇÃO CRIADA POR MIM - Valida o comentário escrito pelo usuário
    private void validarComentario(String username) {

        String textoComentario = editComentario.getText().toString();

        //se o usuário não digitou nada e clicou na imagem de publica o comentario vazio
        if(TextUtils.isEmpty(textoComentario)){
            Toast.makeText(this, "Escreva um comentário, parça", Toast.LENGTH_SHORT).show();
        }else{

            //pegando a data
            Calendar calendarData = Calendar.getInstance();
            SimpleDateFormat dataAtual = new SimpleDateFormat("dd-MM-yyyy");
            final String dataAtualSalva = dataAtual.format(calendarData.getTime());

            //pegando a hora
            Calendar calendarHora = Calendar.getInstance();
            SimpleDateFormat horaAtual = new SimpleDateFormat("HH:mm");
            //vídeo: ele pegou calendarData.getTime()
            final String horaAtualSalva = horaAtual.format(calendarData.getTime());

            final String keyAleatoria = usuarioLogadoId + dataAtualSalva + horaAtualSalva;

            final HashMap comentariosMap = new HashMap();
            comentariosMap.put("uid", usuarioLogadoId);
            comentariosMap.put("comentario", textoComentario);
            comentariosMap.put("data", dataAtualSalva);
            comentariosMap.put("hora", horaAtualSalva);
            comentariosMap.put("username", username);

            postComentariosRef.child(keyAleatoria).updateChildren(comentariosMap).addOnCompleteListener(new OnCompleteListener() {
                @Override
                public void onComplete(@NonNull Task task) {
                    if(task.isSuccessful()){
                        Toast.makeText(ComentariosActivity.this, "comentado com sucesso!", Toast.LENGTH_SHORT).show();
                    }else {
                        Toast.makeText(ComentariosActivity.this, "Ocorreu um erro !" + task.getException().getMessage().toString(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

    }


}