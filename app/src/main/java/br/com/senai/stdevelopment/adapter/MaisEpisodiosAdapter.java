package br.com.senai.stdevelopment.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
//import android.support.v4.view.PagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
//import android.support.v4.view.ViewPager;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import br.com.senai.stdevelopment.PodcastActivity;
import br.com.senai.stdevelopment.R;

/**
 * Created by Acer on 31/12/2020.
 */

public class MaisEpisodiosAdapter extends PagerAdapter {

    Context context;
    LayoutInflater layoutInflater;
    private SliderAdapter sliderAdapter;
    private PodcastActivity podcastActivity;
    private ViewPager viewPagerSlider;

    //construtor
    public MaisEpisodiosAdapter(Context context) {
        this.context = context;
    }


    //Arrays
    //array com todas as imagens dos ep zeros de cada tema
    public int[] epZero = {
            R.drawable.podcast_rosaly,
            R.drawable.podcast_violencia_domestica,
            R.drawable.podcast_divulgacao_cientifica,
            R.drawable.capa_podcast_especiais
    };

    //array com todas as imagens dos ep 1s de cada tema
    public int[] epUm = {
            R.drawable.podcast_indisponivel,
            R.drawable.podcast_racismo_mach,
            R.drawable.podcast_games,
            R.drawable.podcast_indisponivel
    };

    //array com todas os títuls dos ep zeros de cada tema
    public String[] tituloEpZero = {
            "HerScience #00 (ft. Dra. Rosaly Lopes): Luas podem ser habitáveis?",
            "HerScience #00 (ft. Déborah Ravazio&Thays): Violência contra a Mulher",
            "HerScience #00 (ft. Elíade Lime): Como divulgar a ciência?",
            "HerScience #00: ESPECIAL DE NATAL"
    };

    //array com todas os títuls dos ep zeros de cada tema
    public String[] tituloEpUm = {
            "INDISPONÍVEL",
            "HerScience #01 (ft. Sônia Guimarães&Andrea): Esse tal de racismo e machismo",
            "HerScience #01 (ft. Bianca Garcia): Games e as personagens femininas",
            "INDISPONÍVEL"
    };


    @Override
    public int getCount() {
        return epZero.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == (RelativeLayout) object;
    }


    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        final View view = layoutInflater.inflate(R.layout.popup_slider_podcast, container, false);

        final ImageView capaEpZero = (ImageView) view.findViewById(R.id.imgEpEspecialZero);
        final TextView titleEpZero = (TextView) view.findViewById(R.id.txtEpEspecialZero);
        final ImageView capaEpUm = (ImageView) view.findViewById(R.id.imgEpEspecialUm);
        TextView titleEpUm = (TextView) view.findViewById(R.id.txtEpEspecialUm);

        capaEpZero.setImageResource(epZero[position]);
        capaEpUm.setImageResource(epUm[position]);
        titleEpZero.setText(tituloEpZero[position]);
        titleEpZero.setPaintFlags(titleEpZero.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        titleEpUm.setText(tituloEpUm[position]);

        //Ao clicar na capa do ep zero
        capaEpZero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(capaEpZero.equals(R.drawable.capa_podcast_especiais)){
                    Intent intentSpotify = new Intent("android.intent.action.VIEW", Uri.parse("https://open.spotify.com/episode/12FmAYZiXNVP3mG4c4zoxU?si=sWm5LO0LTCWzkSQC-4pTHQ"));
                    view.getContext().startActivity(intentSpotify);
                } else if(capaEpZero.equals(R.drawable.podcast_rosaly)){
                    Intent intentSpotify = new Intent("android.intent.action.VIEW", Uri.parse("https://open.spotify.com/episode/12FmAYZiXNVP3mG4c4zoxU?si=sWm5LO0LTCWzkSQC-4pTHQ"));
                    view.getContext().startActivity(intentSpotify);
                }else if(capaEpZero.equals(R.drawable.podcast_violencia_domestica)){
                    Toast.makeText(context, "Episódio ainda não disponível", Toast.LENGTH_SHORT).show();
                }else if(capaEpZero.equals(R.drawable.podcast_divulgacao_cientifica)){
                    Toast.makeText(context, "Episódio ainda não disponível", Toast.LENGTH_SHORT).show();
                }
            }
        });

        //Ao clicar no título do ep zero
        titleEpZero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(titleEpZero.getText() == "HerScience #00: ESPECIAL DE NATAL"){
                    Intent intentSpotify = new Intent("android.intent.action.VIEW", Uri.parse("https://open.spotify.com/show/7DazVbHOumqq0KGv7jrXez"));
                    view.getContext().startActivity(intentSpotify);
                }else if(titleEpZero.getText() == "HerScience #00 (ft. Dra. Rosaly Lopes): Luas podem ser habitáveis?"){
                    Intent intentSpotify = new Intent("android.intent.action.VIEW", Uri.parse("https://open.spotify.com/episode/12FmAYZiXNVP3mG4c4zoxU?si=sWm5LO0LTCWzkSQC-4pTHQ"));
                    view.getContext().startActivity(intentSpotify);
                }else if(titleEpZero.getText() == "HerScience #00 (ft. Déborah Ravazio&Thays): Violência contra a Mulher"){
                    Toast.makeText(context, "Episódio ainda não disponível", Toast.LENGTH_SHORT).show();
                }else if(titleEpZero.getText() == "HerScience #00 (ft. Elíade Lime): Como divulgar a ciência?"){
                    Toast.makeText(context, "Episódio ainda não disponível", Toast.LENGTH_SHORT).show();
                }
            }
        });

        container.addView(view);


        return view;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout)object);
    }

}
