package br.com.senai.stdevelopment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
//import android.support.annotation.NonNull;
import androidx.annotation.NonNull;
//import android.support.annotation.Nullable;
import androidx.annotation.Nullable;
//import android.support.v4.app.Fragment;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;
import android.widget.Toolbar;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Acer on 04/08/2020.
 */

public class NovoPostFragment extends Fragment {


    private Toolbar toolbar;
    private ImageButton imgNovoPost;
    private EditText textNovoPost;
    private Button btnPost;

    private static  final  int Gallery_Pick = 1;
    private final int result_crop = 2;

    private Uri imgUri;

    private String descricao;
    private StorageReference postReference;
    private String dataSalva, horaSalva, nomeRandomPost, downloadUrl, usuarioId;

    private DatabaseReference usuarioReference, postBaseReference;
    private FirebaseAuth auth;

    private ProgressDialog loading;


    private CheckBox checkFilosofia;
    private CheckBox checkSociologia;
    private CheckBox checkQuimica;
    private CheckBox checkBiologia;
    private CheckBox checkFisica;
    private CheckBox checkMatematica;
    private CheckBox checkPolitica;
    private CheckBox checkProgramacao;
    private CheckBox checkHistoria;
    private CheckBox checkGeografia;


    private boolean matematica;
    private boolean fisica;
    private boolean biologia;
    private boolean quimica;
    private boolean historia;
    private boolean geografia;
    private boolean filosofia;
    private boolean sociologia;
    private boolean programacao;
    private boolean politica;

    private long contaPosts = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.activity_post, container, false);



        imgNovoPost = (ImageButton) v.findViewById(R.id.imgNovoPost);
        textNovoPost = (EditText) v.findViewById(R.id.ediTextNovoPost);
        btnPost = (Button) v.findViewById(R.id.btnNovoPost);

        postReference = FirebaseStorage.getInstance().getReference();
        usuarioReference = FirebaseDatabase.getInstance().getReference().child("users").child("User");
        auth = FirebaseAuth.getInstance();
        usuarioId = auth.getCurrentUser().getUid();

        postBaseReference = FirebaseDatabase.getInstance().getReference().child("users").child("Post");


        checkFilosofia = (CheckBox) v.findViewById(R.id.checkFilosofia);
        checkSociologia = (CheckBox) v.findViewById(R.id.checkSociologia);
        checkBiologia = (CheckBox) v.findViewById(R.id.checkBiologia);
        checkQuimica = (CheckBox) v.findViewById(R.id.checkQuimica);
        checkFisica = (CheckBox) v.findViewById(R.id.checkFisica);
        checkMatematica = (CheckBox) v.findViewById(R.id.checkMatematica);
        checkHistoria = (CheckBox) v.findViewById(R.id.checkHistoria);
        checkGeografia = (CheckBox) v.findViewById(R.id.checkGeografia);
        checkProgramacao = (CheckBox) v.findViewById(R.id.checkProgramacao);
        checkPolitica = (CheckBox) v.findViewById(R.id.checkPolitica);



        loading = new ProgressDialog(getContext());

        imgNovoPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OpenGallery();
            }
        });

        btnPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (checkFisica.isChecked()){
                    fisica = true;
                    Toast.makeText(getContext(), "clicou", Toast.LENGTH_LONG).show();
                } else{
                    fisica = false;
                }

                if (checkGeografia.isChecked()){
                    geografia = true;
                } else{
                    geografia = false;
                }


                if (checkHistoria.isChecked()){
                    historia = true;
                } else{
                    historia = false;
                }

                if(checkBiologia.isChecked()){
                    biologia = true;
                }else{
                    biologia = false;
                }


                if(checkQuimica.isChecked()){
                    quimica = true;
                }else{
                    quimica = false;
                }


                if(checkFilosofia.isChecked()){
                    filosofia = true;
                }else {
                    filosofia = false;
                }


                if(checkSociologia.isChecked()){
                    sociologia = true;
                }else {
                    sociologia = false;
                }

                if(checkProgramacao.isChecked()){
                    programacao = true;
                }else {
                    programacao = false;
                }

                if(checkMatematica.isChecked()){
                    matematica = true;
                }else {
                    matematica = false;
                }


                ValidatePostInfo();
            }
        });

        return v;
    }



    // MÉTODO CRIADO POR MIM - valida os campos antes de salvar
    private void ValidatePostInfo() {
        descricao = textNovoPost.getText().toString();

        //se a pessoa não selecionou uma imagem para o post
        if(imgUri == null){
            Toast.makeText(getContext(), "Não vai nenhuma imagem?", Toast.LENGTH_SHORT).show();
        }
        //se a pessoa não escreveu nenhum texto para o post
        else if(TextUtils.isEmpty(descricao)){
            Toast.makeText(getContext(), "Cade o textinho??", Toast.LENGTH_SHORT).show();
        } else{

            loading.setTitle("Salvando Post");
            loading.setMessage("aguarda ai rapa");
            loading.show();
            loading.setCanceledOnTouchOutside(true);

            StoringImageToFirebaseStorage();
        }

    }

    // MÉTODO CRIADO POR MIM - salva na FireBase
    private void StoringImageToFirebaseStorage() {

        //pegando a data
        Calendar calendarData = Calendar.getInstance();
        SimpleDateFormat dataAtual = new SimpleDateFormat("dd-MM-yyyy");
        dataSalva = dataAtual.format(calendarData.getTime());


        //pegando a hora
        Calendar calendarHora = Calendar.getInstance();
        SimpleDateFormat horaAtual = new SimpleDateFormat("HH:mm");
        //vídeo: ele pegou calendarData.getTime()
        horaSalva = horaAtual.format(calendarData.getTime());

        //criando um nome aleatorio para cada imagem do post de um determinado usuario, para ser unico
        nomeRandomPost = dataSalva+horaSalva;


        StorageReference filePath = postReference.child("Post Images").child(imgUri.getLastPathSegment() + nomeRandomPost + ".jpg");

        // Comprime a imagem do post
        byte[] imgCompressaBytes = null;
        try {
            Bitmap bmpImg = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), imgUri);
            ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
            bmpImg.compress(Bitmap.CompressFormat.JPEG, 50, byteStream);
            imgCompressaBytes = byteStream.toByteArray();
        } catch (IOException e) {
            Toast.makeText(getContext(), "Ops! Algo aconteceu. " + e.toString(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

        // Salva a imagem compressa no Firebase

        filePath.putBytes(imgCompressaBytes).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                filePath.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        downloadUrl = uri.toString();
                        Toast.makeText(getActivity(), "Imagem salva com sucesso!", Toast.LENGTH_SHORT).show();

                        //Salvando efetivamente o post:
                        SavingPostInformationToDatabase();
                    }
                });
            }

        });
    }

    //MÉTODO CRIADO POR MIM - salvando  post na Firebase
    private void SavingPostInformationToDatabase() {
        postBaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    contaPosts = dataSnapshot.getChildrenCount();
                }else{
                    contaPosts = 0;
                }
                usuarioReference.child(usuarioId).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if(dataSnapshot.exists()){
                            String usuarioNome = dataSnapshot.child("username").getValue().toString();
                            String usuarioFoto = dataSnapshot.child("profileImg").getValue().toString();

                            HashMap postMap = new HashMap();
                            postMap.put("uid", usuarioId);
                            postMap.put("data", dataSalva);
                            postMap.put("hora", horaSalva);
                            postMap.put("descricao", descricao);
                            postMap.put("imagemPost", downloadUrl);
                            postMap.put("usuarioFoto", usuarioFoto);
                            postMap.put("usuarioNome", usuarioNome);
                            postMap.put("contador", contaPosts);

                            Map<String, Boolean> Likes = new HashMap<>();

                            if(filosofia == true){
                                Likes.put("filosofia", true);
                            }else{
                                Likes.put("filosofia", false);
                            }


                            if(sociologia == true){
                                Likes.put("sociologia", true);
                            }else{
                                Likes.put("sociologia", false);
                            }

                            if(quimica == true){
                                Likes.put("quimica", true);
                            }else{
                                Likes.put("quimica", false);
                            }


                            if(biologia == true){
                                Likes.put("biologia", true);
                            }else{
                                Likes.put("biologia", false);
                            }


                            if(fisica == true){
                                Likes.put("fisica", true);
                            }else{
                                Likes.put("fisica", false);
                            }


                            if(matematica == true){
                                Likes.put("matematica", true);
                            }else{
                                Likes.put("matematica", false);
                            }



                            if(historia == true){
                                Likes.put("historia", true);
                            }else{
                                Likes.put("historia", false);
                            }


                            if(geografia == true){
                                Likes.put("geografia", true);
                            }else{
                                Likes.put("geografia", false);
                            }

                            if(programacao == true){
                                Likes.put("programacao", true);
                            }else{
                                Likes.put("programacao", false);
                            }


                            if(politica == true){
                                Likes.put("politica", true);
                            }else{
                                Likes.put("politica", false);
                            }


                            postMap.put("Likes",Likes);


                            postBaseReference.child(usuarioId+nomeRandomPost).updateChildren(postMap).addOnCompleteListener(new OnCompleteListener() {
                                @Override
                                public void onComplete(@NonNull Task task) {
                                    if(task.isSuccessful()){

                                        Intent intent = new Intent(getActivity(), MediaEntradaActivity.class);
                                        startActivity(intent);

                                        Toast.makeText(getActivity(), "td ok!", Toast.LENGTH_SHORT).show();
                                        loading.dismiss();
                                        getActivity().finish();

                                    } else{
                                        String msg = task.getException().getMessage();
                                        Toast.makeText(getActivity(), "eita: "+ msg, Toast.LENGTH_SHORT).show();
                                        loading.dismiss();
                                    }
                                }
                            });
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Toast.makeText(getActivity(), "databaseError: "+ databaseError.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(getActivity(), "databaseError: "+ databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    //MÉTODO CRIADO POR MIM - pegando imagem da galeria do seu celular
    private void OpenGallery() {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, Gallery_Pick);
    }

    //salvando a imagem
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == Gallery_Pick && resultCode == Activity.RESULT_OK && data != null){
            imgUri = data.getData();
            imgNovoPost.setImageURI(imgUri);
        }
    }
}