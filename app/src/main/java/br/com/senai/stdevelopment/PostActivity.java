package br.com.senai.stdevelopment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.preference.PreferenceManager;
//import android.support.annotation.NonNull;
import androidx.annotation.NonNull;
//import android.support.v7.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;
import android.widget.Toolbar;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class PostActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private ImageButton imgNovoPost;
    private EditText textNovoPost;
    private Button btnPost;

    private static  final  int Gallery_Pick = 1;

    private Uri imgUri;

    private String descricao;
    private StorageReference postReference;
    private String dataSalva, horaSalva, nomeRandomPost, downloadUrl, usuarioId;

    private DatabaseReference usuarioReference, postBaseReference;
    private FirebaseAuth auth;

    private ProgressDialog loading;


    private CheckBox checkFilosofia;
    private CheckBox checkSociologia;
    private CheckBox checkQuimica;
    private CheckBox checkBiologia;
    private CheckBox checkFisica;
    private CheckBox checkMatematica;
    private CheckBox checkPolitica;
    private CheckBox checkProgramacao;
    private CheckBox checkHistoria;
    private CheckBox checkGeografia;


    private boolean matematica;
    private boolean fisica;
    private boolean biologia;
    private boolean quimica;
    private boolean historia;
    private boolean geografia;
    private boolean filosofia;
    private boolean sociologia;
    private boolean programacao;
    private boolean politica;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);

        imgNovoPost = (ImageButton) findViewById(R.id.imgNovoPost);
        textNovoPost = (EditText) findViewById(R.id.ediTextNovoPost);
        btnPost = (Button) findViewById(R.id.btnNovoPost);

        postReference = FirebaseStorage.getInstance().getReference();
        usuarioReference = FirebaseDatabase.getInstance().getReference().child("users").child("User");
        auth = FirebaseAuth.getInstance();
        usuarioId = auth.getCurrentUser().getUid();


        checkFilosofia = (CheckBox) findViewById(R.id.checkFilosofia);
        checkSociologia = (CheckBox) findViewById(R.id.checkSociologia);
        checkBiologia = (CheckBox) findViewById(R.id.checkBiologia);
        checkQuimica = (CheckBox) findViewById(R.id.checkQuimica);
        checkFisica = (CheckBox) findViewById(R.id.checkFisica);
        checkMatematica = (CheckBox) findViewById(R.id.checkMatematica);
        checkHistoria = (CheckBox) findViewById(R.id.checkHistoria);
        checkGeografia = (CheckBox) findViewById(R.id.checkGeografia);
        checkProgramacao = (CheckBox) findViewById(R.id.checkProgramacao);
        checkPolitica = (CheckBox) findViewById(R.id.checkPolitica);


        postBaseReference = FirebaseDatabase.getInstance().getReference().child("users").child("Post");

        loading = new ProgressDialog(this);

        imgNovoPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OpenGallery();
            }
        });

        btnPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkFisica.isChecked()){
                    fisica = true;
                    Toast.makeText(PostActivity.this, "clicou", Toast.LENGTH_LONG).show();
                } else{
                    fisica = false;
                }

                if (checkGeografia.isChecked()){
                    geografia = true;
                } else{
                    geografia = false;
                }


                if (checkHistoria.isChecked()){
                    historia = true;
                } else{
                    historia = false;
                }

                if(checkBiologia.isChecked()){
                    biologia = true;
                }else{
                    biologia = false;
                }


                if(checkQuimica.isChecked()){
                    quimica = true;
                }else{
                    quimica = false;
                }


                if(checkFilosofia.isChecked()){
                    filosofia = true;
                }else {
                    filosofia = false;
                }


                if(checkSociologia.isChecked()){
                    sociologia = true;
                }else {
                    sociologia = false;
                }

                if(checkProgramacao.isChecked()){
                    programacao = true;
                }else {
                    programacao = false;
                }

                if(checkMatematica.isChecked()){
                    matematica = true;
                }else {
                    matematica = false;
                }



                ValidatePostInfo();
            }
        });
    }



    // MÉTODO CRIADO POR MIM - valida os campos antes de salvar
    private void ValidatePostInfo() {
        descricao = textNovoPost.getText().toString();

        //se a pessoa não selecionou uma imagem para o post
        if(imgUri == null){
            Toast.makeText(this, "Não vai nenhuma imagem?", Toast.LENGTH_SHORT).show();
        }
        //se a pessoa não escreveu nenhum texto para o post
        else if(TextUtils.isEmpty(descricao)){
            Toast.makeText(this, "Cade o textinho??", Toast.LENGTH_SHORT).show();
        } else{

            loading.setTitle("Salvando Post");
            loading.setMessage("aguarda ai rapa");
            loading.show();
            loading.setCanceledOnTouchOutside(true);

            StoringImageToFirebaseStorage();
        }

    }



    // MÉTODO CRIADO POR MIM - salva na FireBase
    private void StoringImageToFirebaseStorage() {

        //pegando a data
        Calendar calendarData = Calendar.getInstance();
        SimpleDateFormat dataAtual = new SimpleDateFormat("dd-MM-yyyy");
        dataSalva = dataAtual.format(calendarData.getTime());


        //pegando a hora
        Calendar calendarHora = Calendar.getInstance();
        SimpleDateFormat horaAtual = new SimpleDateFormat("HH:mm");
        //vídeo: ele pegou calendarData.getTime()
        horaSalva = horaAtual.format(calendarData.getTime());

        //criando um nome aleatorio para cada imagem do post de um determinado usuario, para ser unico
        nomeRandomPost = dataSalva+horaSalva;


        StorageReference filePath = postReference.child("Post Images").child(imgUri.getLastPathSegment() + nomeRandomPost + ".jpg");

        // Comprime a imagem do post
        byte[] imgCompressaBytes = null;
        try {
            Bitmap bmpImg = MediaStore.Images.Media.getBitmap(getContentResolver(), imgUri);
            ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
            bmpImg.compress(Bitmap.CompressFormat.JPEG, 50, byteStream);
            imgCompressaBytes = byteStream.toByteArray();
        } catch (IOException e) {
            Toast.makeText(this, "Ops! Algo aconteceu. " + e.toString(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

        // Salva a imagem compressa no Firebase

        //Salvar a imagem na Firebase
        filePath.putFile(imgUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                filePath.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        downloadUrl = uri.toString();
                        Toast.makeText(PostActivity.this, "Imagem salva com sucesso!", Toast.LENGTH_SHORT).show();

                        //Salvando efetivamente o post:
                        SavingPostInformationToDatabase();
                    }
                });
            }

        });
    }


    //MÉTODO CRIADO POR MIM - salvando  post na Firebase
    private void SavingPostInformationToDatabase() {

        usuarioReference.child(usuarioId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if(dataSnapshot.exists()){
                    String usuarioNome = dataSnapshot.child("username").getValue().toString();
                    String usuarioFoto = dataSnapshot.child("profileImg").getValue().toString();

                    HashMap postMap = new HashMap();
                    postMap.put("uid", usuarioId);
                    postMap.put("data", dataSalva);
                    postMap.put("hora", horaSalva);
                    postMap.put("descricao", descricao);
                    postMap.put("imagemPost", downloadUrl);
                    postMap.put("usuarioFoto", usuarioFoto);
                    postMap.put("usuarioNome", usuarioNome);


                    //Salvando os likes do usuario
/*                    Map<String, Boolean> likes = new HashMap<>();

                    if(filosofia == true){
                        likes.put("filosofia", true);
                    }else{
                        likes.put("filosofia", false);
                    }


                    if(sociologia == true){
                        likes.put("sociologia", true);
                    }else{
                        likes.put("sociologia", false);
                    }

                    if(quimica == true){
                        likes.put("quimica", true);
                    }else{
                        likes.put("quimica", false);
                    }


                    if(biologia == true){
                        likes.put("biologia", true);
                    }else{
                        likes.put("biologia", false);
                    }


                    if(fisica == true){
                        likes.put("fisica", true);
                    }else{
                        likes.put("fisica", false);
                    }


                    if(matematica == true){
                        likes.put("matematica", true);
                    }else{
                        likes.put("matematica", false);
                    }



                    if(historia == true){
                        likes.put("historia", true);
                    }else{
                        likes.put("historia", false);
                    }


                    if(geografia == true){
                        likes.put("geografia", true);
                    }else{
                        likes.put("geografia", false);
                    }

                    if(programacao == true){
                        likes.put("programacao", true);
                    }else{
                        likes.put("programacao", false);
                    }


                    if(politica == true){
                        likes.put("politica", true);
                    }else{
                        likes.put("politica", false);
                    }


                    postMap.put("Likes",likes);*/


                    postBaseReference.child(usuarioId+nomeRandomPost).updateChildren(postMap).addOnCompleteListener(new OnCompleteListener() {
                        @Override
                        public void onComplete(@NonNull Task task) {
                            if(task.isSuccessful()){

//                                salvarTagsPost();

                                Intent intent = new Intent(PostActivity.this, MediaEntradaActivity.class);
                                startActivity(intent);

                                Toast.makeText(PostActivity.this, "td ok!", Toast.LENGTH_SHORT).show();
                                loading.dismiss();
                                finish();

                            } else{
                                String msg = task.getException().getMessage();
                                Toast.makeText(PostActivity.this, "eita: "+ msg, Toast.LENGTH_SHORT).show();
                                loading.dismiss();
                            }
                        }
                    });


                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


    //MÉTODO CRIADO POR MIM - pegando imagem da galeria do seu celular
    private void OpenGallery() {

        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, Gallery_Pick);

    }


    //salvando a imagem
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == Gallery_Pick && resultCode == RESULT_OK && data != null){
            imgUri = data.getData();
            imgNovoPost.setImageURI(imgUri);
        }

    }
}