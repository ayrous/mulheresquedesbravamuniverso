package br.com.senai.stdevelopment.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
//import android.support.annotation.RequiresApi;
import androidx.annotation.RequiresApi;
//import android.support.v4.view.PagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import br.com.senai.stdevelopment.R;
import br.com.senai.stdevelopment.MaisEpisodios;

/**
 * Created by Acer on 30/06/2020.
 */

public class SliderAdapter extends PagerAdapter {

    Context context;
    LayoutInflater layoutInflater;


    //construtor
    public SliderAdapter(Context context) {
        this.context = context;
    }

    //Arrays
    public int[] sliderImages = {
        R.drawable.podcast_rosaly,
        R.drawable.podcast_violencia_domestica,
        R.drawable.podcast_divulgacao_cientifica,
            R.drawable.capa_podcast_especiais
    };

    public String[] sliderCabecalhos = {
            "VIDA FORA DA TERRA",
            "QUESTÃO DE GÊNERO?",
            "DIVULGAÇÃO CIENTÍFICA",
            "ESPECIAIS HERSCIENCE"
    };

    public String[] descricoes = {
            "Será que somos os únicos?",
            "Impactos do preconceito na sociedade",
            "os 5 níveis de divulgar a ciência",
            "Por e para vocês!"
    };


    @Override
    public int getCount() {
        return sliderCabecalhos.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == (RelativeLayout) object;
    }

    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        final View view = layoutInflater.inflate(R.layout.slide_layout, container, false);

        final ImageView slideImageView = (ImageView) view.findViewById(R.id.iconUmSlider);
//        final TextView slideCabeçalho = (TextView) view.findViewById(R.id.tituloSliderUm);
        //final Button btnAbrirMaisEp = (Button) view.findViewById(R.id.btnPodcastVerEp);
        final Button btnAbrirMaisEp = (Button) view.findViewById(R.id.btnPodcastVerEp);
        TextView slideDescricao = (TextView) view.findViewById(R.id.subSliderUm);

        slideImageView.setImageResource(sliderImages[position]);
        btnAbrirMaisEp.setText(sliderCabecalhos[position]);
        slideDescricao.setText(descricoes[position]);

        container.addView(view);

        //Ao clicar no ícone
        slideImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(slideImageView.equals(R.drawable.capa_podcast_especiais)){
                    Intent intentSpotify = new Intent("android.intent.action.VIEW", Uri.parse("https://open.spotify.com/show/7DazVbHOumqq0KGv7jrXez"));
                    view.getContext().startActivity(intentSpotify);
                }else if(slideImageView.equals(R.drawable.podcast_rosaly)){
                    Intent intentSpotify = new Intent("android.intent.action.VIEW", Uri.parse("https://open.spotify.com/episode/12FmAYZiXNVP3mG4c4zoxU?si=sWm5LO0LTCWzkSQC-4pTHQ"));
                    view.getContext().startActivity(intentSpotify);
                }else if(slideImageView.equals(R.drawable.podcast_violencia_domestica)){
                    Toast.makeText(context, "Episódio ainda não disponível", Toast.LENGTH_SHORT).show();
                }else if(slideImageView.equals(R.drawable.podcast_divulgacao_cientifica)){
                    Toast.makeText(context, "Episódio ainda não disponível", Toast.LENGTH_SHORT).show();
                }
            }
        });


        btnAbrirMaisEp.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context.getApplicationContext(), MaisEpisodios.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("posicaoSlider", position);
                context.getApplicationContext().startActivity(intent);
            }
        });

        return view;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout)object);
    }
}
