package br.com.senai.stdevelopment;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
//import android.support.annotation.RequiresApi;
import androidx.annotation.RequiresApi;
//import android.support.design.widget.FloatingActionButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
//import android.support.design.widget.Snackbar;
import com.google.android.material.snackbar.Snackbar;
import android.util.Log;
import android.view.View;
//import android.support.design.widget.NavigationView;
import com.google.android.material.navigation.NavigationView;
//import android.support.v4.view.GravityCompat;
import androidx.core.view.GravityCompat;
//import android.support.v4.widget.DrawerLayout;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
//import android.support.v7.app.ActionBarDrawerToggle;
//import androidx.legacy.app.ActionBarDrawerToggle;
//import android.support.v7.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatActivity;
//import android.support.v7.widget.Toolbar;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

import br.com.senai.stdevelopment.modelo.Mensagem;
import br.com.senai.stdevelopment.modelo.MensagemDAO;
import br.com.senai.stdevelopment.utils.Mail;

public class ContatoActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private Button enviar;
    private EditText sugestoes, numeroTel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contato);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        enviar = (Button) findViewById(R.id.btnEnviar);
        sugestoes = (EditText) findViewById(R.id.editSugestoes);
        numeroTel = (EditText) findViewById(R.id.editTelefone);

        final MensagemHelper help = new MensagemHelper(ContatoActivity.this);
        Intent intent = getIntent();
        final Mensagem mensagem = (Mensagem) intent.getSerializableExtra("mensagens");

        if (mensagem != null) {
            help.preencherMensagem(mensagem);
        }

        enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(sugestoes.getText().length() != 0){
                    Intent intent = new Intent(ContatoActivity.this, MensagemActivity.class);
                    finish();
                    startActivity(intent);
                    Mensagem mensagem1 = help.pegarMensagem();
                    MensagemDAO dao = new MensagemDAO(ContatoActivity.this);

                    if (mensagem1.getId() != null) {
                        dao.editar(mensagem1);
                        Toast.makeText(getApplicationContext(), "Sua mensagem foi editada com sucesso!", Toast.LENGTH_LONG).show();
                    } else {
                        dao.salvar(mensagem1);

                        if(numeroTel.getText().toString() != null){
                            enviarEmail(mensagem1.getMensagem(), numeroTel.getText().toString());
                        }else{
                            enviarEmail(mensagem1.getMensagem(), "sem numero");
                        }
                        Toast.makeText(getApplicationContext(), "Sua mensagem foi enviada com sucesso!", Toast.LENGTH_LONG).show();
                    }
                    dao.close();

                    finish();
                }else{
                    Toast.makeText(getApplicationContext(), "Insira uma mensagem!", Toast.LENGTH_LONG).show();
                }
            }
        });


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ContatoActivity.this, MensagemActivity.class);
                startActivity(intent);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.contato, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Toast.makeText(getApplicationContext(), "Opção desabilitada!", Toast.LENGTH_LONG).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_logout3) {
            Toast.makeText(getApplicationContext(), "Volte sempre!", Toast.LENGTH_LONG).show();
            FirebaseAuth.getInstance().signOut();
            finishAffinity();
            startActivity(new Intent(ContatoActivity.this, TelaPrincipalActivity.class));
            // Handle the camera action
        } else if (id == R.id.nav_sobreNos3) {
            Intent intent = new Intent(ContatoActivity.this, SobreNosActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_historia3) {
            Intent intent = new Intent(ContatoActivity.this, HistoriaActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_contato3) {
            Toast.makeText(getApplicationContext(), "Você já está nessa tela!", Toast.LENGTH_LONG).show();

        } else if (id == R.id.nav_mensagem3) {
            Intent intent = new Intent(ContatoActivity.this, MensagemActivity.class);
            startActivity(intent);

        } else if(id ==  R.id.nav_yout3){
            Toast.makeText(getApplication(), "Você será redirecionado!", Toast.LENGTH_LONG).show();
            Uri uri = Uri.parse("https://www.youtube.com/channel/UCVask_OFnc7MeYMWVV86M-Q");
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    //Função CRIADA POR MIM: enviar emails para o gmail de suporte
    private void enviarEmail(String msg, String numero){

        final String email = "desbravadoras.do.universo@gmail.com";
        final String subject = "[SUPORTE APP]: " + numero;
        final String body = msg;

        if(!isOnline()) {
            Toast.makeText(getApplicationContext(), "Não estava online para enviar e-mail!", Toast.LENGTH_SHORT).show();
            System.exit(0);
        }


        new Thread(new Runnable(){
            @Override
            public void run() {
                Mail m = new Mail();

                String[] toArr = {email};
                m.setTo(toArr);

                //m.setFrom("seunome@seuemail.com.br"); //caso queira enviar em nome de outro
                m.setSubject(subject);
                m.setBody(body);

                try {
                    //m.addAttachment("pathDoAnexo");//anexo opcional
                    m.send();
                }
                catch(RuntimeException rex){ }//erro ignorado
                catch(Exception e) {
                    e.printStackTrace();
                    System.exit(0);
                }

//                final Toast toast = Toast.makeText(getApplicationContext(), "foiii", 20);
 //               toast.show();
            }
        }).start();
    }


    public boolean isOnline() {
        try {
            ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            return netInfo != null && netInfo.isConnectedOrConnecting();
        }
        catch(Exception ex){
            Toast.makeText(getApplicationContext(), "Erro ao verificar se estava online! (" + ex.getMessage() + ")", Toast.LENGTH_SHORT).show();
            return false;
        }
    }
}
