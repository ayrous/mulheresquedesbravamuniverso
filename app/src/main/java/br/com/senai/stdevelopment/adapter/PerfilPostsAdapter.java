package br.com.senai.stdevelopment.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.senai.stdevelopment.R;
import br.com.senai.stdevelopment.modelo.Posts;

public class PerfilPostsAdapter extends BaseAdapter {
    
    private Context context;
    private LayoutInflater layoutInflater;
    private List<Posts> listaPosts;

    public PerfilPostsAdapter(Context context, List<Posts> listaPosts) {
        this.context = context;
        this.listaPosts = listaPosts;
    }

    @Override
    public int getCount() {
        return listaPosts.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        if (layoutInflater == null){
            layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if (view == null){
            view = layoutInflater.inflate(R.layout.posts_perfil, viewGroup, false);
        }

        ImageView imgPost = view.findViewById(R.id.imgPostPerfil);
        Posts posts = listaPosts.get(i);

        int width= context.getResources().getDisplayMetrics().widthPixels;
        Picasso.with(context).load(posts.getImagemPost()).centerCrop().resize(width/2,width/2).into(imgPost);

      //  postInformacoes(imgPost, posts.getUid());

        return view;
    }




    //FUNÇÃO CRIADA POR MIM -> CARREGA INFORMAÇÕES DOS POSTS DO USUÁRIO
    private  void postInformacoes(final ImageView imgPst, String userId){

        Toast.makeText(context, "   POST INFO", Toast.LENGTH_SHORT).show();

        DatabaseReference reference = FirebaseDatabase.getInstance().getReference().child("users").child("Post");
        Query ordenarPostsUsuario = reference.orderByChild("uid").equalTo(userId);

        ordenarPostsUsuario.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                //Setando a foto de perfil
                String imagemPostBanco = dataSnapshot.child("imagemPost").getValue().toString();

                Toast.makeText(context, "PASSEEI", Toast.LENGTH_SHORT).show();

                Picasso.with(context).load(imagemPostBanco).into(imgPst);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


}
